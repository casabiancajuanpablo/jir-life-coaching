<?php
add_action( 'wp_enqueue_scripts', 'schon_child_theme_enqueue_styles', 99 );
function schon_child_theme_enqueue_styles() {
	$style_require = array();
	if(wp_script_is('grid-list-layout'))
		array_push($style_require, 'grid-list-layout');
	if(wp_script_is('grid-list-layout'))
		array_push($style_require, 'grid-list-button');

	array_push($style_require, 'bootstrap');

	wp_enqueue_style( 'schon-parent', get_template_directory_uri() . '/style.css', $style_require );
	wp_dequeue_style( 'theme-options');
	wp_enqueue_style('theme-options', get_template_directory_uri() . "/css/theme-options.css", array('schon-parent'));
	wp_enqueue_style( 'schon-child', get_stylesheet_uri(), array('schon', 'theme-options'));
}