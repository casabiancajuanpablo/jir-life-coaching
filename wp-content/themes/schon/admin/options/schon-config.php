<?php

/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
	return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "schon_options";

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

/*
//Todo vedere se serve o eliminare
// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns      = array();

if ( is_dir( $sample_patterns_path ) ) {

	if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
		$sample_patterns = array();

		while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

			if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
				$name              = explode( '.', $sample_patterns_file );
				$name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
				$sample_patterns[] = array(
					'alt' => $name,
					'img' => $sample_patterns_url . $sample_patterns_file
				);
			}
		}
	}
}*/

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
	// TYPICAL -> Change these values as you need/desire
	'opt_name'             => $opt_name,
	// This is where your data is stored in the database and also becomes your global variable name.
	'display_name'         => $theme->get( 'Name' ),
	// Name that appears at the top of your panel
	'display_version'      => $theme->get( 'Version' ),
	// Version that appears at the top of your panel
	'menu_type'            => 'menu',
	//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
	'allow_sub_menu'       => true,
	// Show the sections below the admin menu item or not
	'menu_title'           => esc_html__( 'Schon Options', 'schon' ),
	'page_title'           => esc_html__( 'Schon Options', 'schon' ),
	// You will need to generate a Google API key to use this feature.
	// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
	'google_api_key'       => '',
	// Set it you want google fonts to update weekly. A google_api_key value is required.
	'google_update_weekly' => false,
	// Must be defined to add google fonts to the typography module
	'async_typography'     => true,
	// Use a asynchronous font on the front end or font string
	//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
	'admin_bar'            => true,
	// Show the panel pages on the admin bar
	'admin_bar_icon'       => 'dashicons-portfolio',
	// Choose an icon for the admin bar menu
	'admin_bar_priority'   => 50,
	// Choose an priority for the admin bar menu
	'global_variable'      => '',
	// Set a different name for your global variable other than the opt_name
	'dev_mode'             => false,
	// Show the time the page took to load, etc
	'update_notice'        => true,
	// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
	'customizer'           => true,
	// Enable basic customizer support
	//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
	//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

	// OPTIONAL -> Give you extra features
	'page_priority'        => null,
	// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	'page_parent'          => 'themes.php',
	// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	'page_permissions'     => 'manage_options',
	// Permissions needed to access the options panel.
	'menu_icon'            => '',
	// Specify a custom URL to an icon
	'last_tab'             => '',
	// Force your panel to always open to a specific tab (by id)
	'page_icon'            => 'icon-themes',
	// Icon displayed in the admin panel next to your menu_title
	'page_slug'            => '_options',
	// Page slug used to denote the panel
	'save_defaults'        => true,
	// On load save the defaults to DB before user clicks save or not
	'default_show'         => false,
	// If true, shows the default value next to each field that is not the default value.
	'default_mark'         => '',
	// What to print by the field's title if the value shown is default. Suggested: *
	'show_import_export'   => true,
	// Shows the Import/Export panel when not used as a field.
	'show_options_object' => false,
	// CAREFUL -> These options are for advanced use only
	'transient_time'       => 60 * MINUTE_IN_SECONDS,
	'output'               => true,
	// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	'output_tag'           => true,
	// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

	// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	'database'             => '',
	// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

	'use_cdn'              => true,
	// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

	//'compiler'             => true,

	// HINTS
	'hints'                => array(
		'icon'          => 'el el-question-sign',
		'icon_position' => 'right',
		'icon_color'    => 'lightgray',
		'icon_size'     => 'normal',
		'tip_style'     => array(
			'color'   => 'light',
			'shadow'  => true,
			'rounded' => false,
			'style'   => '',
		),
		'tip_position'  => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect'    => array(
			'show' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'mouseover',
			),
			'hide' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'click mouseleave',
			),
		),
	)
);

//Todo aggiungere socials e link to documentation
/*
$args['admin_bar_links'][] = array(
	'id'    => 'redux-docs',
	'href'  => 'http://docs.reduxframework.com/',
	'title' => esc_html__( 'Documentation', 'schon' ),
);

$args['admin_bar_links'][] = array(
	//'id'    => 'redux-support',
	'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
	'title' => esc_html__( 'Support', 'schon' ),
);

$args['admin_bar_links'][] = array(
	'id'    => 'redux-extensions',
	'href'  => 'reduxframework.com/extensions',
	'title' => esc_html__( 'Extensions', 'schon' ),
);

$args['share_icons'][] = array(
	'url'   => 'https://www.facebook.com/webbaku',
	'title' => 'Like us on Facebook',
	'icon'  => 'el-icon-facebook'
);
$args['share_icons'][] = array(
	'url'   => 'https://twitter.com/webbaku',
	'title' => 'Follow us on Twitter',
	'icon'  => 'el-icon-twitter'
);
$args['share_icons'][] = array(
	'url'   => 'https://plus.google.com/+Webbaku/about',
	'title' => 'Find us on Google+',
	'icon'  => 'el-icon-googleplus'
);
$args['share_icons'][] = array(
	'url'   => 'https://dribbble.com/webbaku',
	'title' => 'Find us on Dribbble',
	'icon'  => 'el-icon-dribbble'
);*/

// Panel Intro text -> before the form
if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
	if ( ! empty( $args['global_variable'] ) ) {
		$v = $args['global_variable'];
	} else {
		$v = str_replace( '-', '_', $args['opt_name'] );
	}
//	$args['intro_text'] = sprintf( esc_html__( '<p>ITALIANO: Eventuale testo sopra il pannello</p>', 'schon' ), $v );
} else {
//	$args['intro_text'] = esc_html__( '<p>ITALIANO: Eventuale testo sopra il pannello</p>', 'schon' );
}

// Add content after the form.
//$args['footer_text'] = esc_html__( '<p>ITALIANO: Eventuale testo sotto il pannello</p>', 'schon' );

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */

/*
 * ---> START HELP TABS
 */
$tabs = array(
	array(
		'id'      => 'redux-help-tab-1',
		'title'   => esc_html__( 'Theme Information 1', 'schon' ),
		'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'schon' )
	),
	array(
		'id'      => 'redux-help-tab-2',
		'title'   => esc_html__( 'Theme Information 2', 'schon' ),
		'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'schon' )
	)
);
Redux::setHelpTab( $opt_name, $tabs );

// Set the help sidebar
$content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'schon' );
Redux::setHelpSidebar( $opt_name, $content );


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*
	As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for

 */


/*
 * <--- END SECTIONS
 */

// GENERAL DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'General', 'schon' ),
	'id' => 'general',
	'fields' => array(

		array(
			'id'   => 'info-logo-favicon',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Logo and Favicon', 'schon')
		),

		array(
			'id'       => 'custom-favicon',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__('Favicon', 'schon'),
			'default'  => array(
				'url'=>''
			)
		),

		array(
			'id'       => 'name-logo',
			'type'     => 'switch',
			'title'    => esc_html__('Name Logo', 'schon'),
			'subtitle'    => esc_html__('Use website name instead of graphic logo', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'logo-image',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__('Logo Image', 'schon'),
			'default'  => array(
				'url'=> get_template_directory_uri() . '/admin/options/img/logo.png',
			),
			'required' => array(
				array('name-logo','=',false),
			)
		),
		array(
			'id'       => 'retina-logo-image',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__('Retina Logo Image', 'schon'),
			'default'  => array(
				'url'=> get_template_directory_uri() . '/admin/options/img/logo.png',
			),
			'required' => array(
				array('name-logo','=',false),
			)
		),

		array(
			'id'          => 'typography-text-logo',
			'type'        => 'typography',
			'title'       => esc_html__('Logo Typography', 'schon'),
			'google'      => true,
			'font-backup' => true,
			'line-height' => false,
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => true,
			'subsets'     => false,
			//'line-height' => false,
			'color'       => true,
			'letter-spacing' => false,
			'compiler'      => array('.site-brand .site-title, .site-brand .site-title a'),
			'required' => array(
				array('name-logo','=',true),
			)
		),

		array(
			'id'       => 'logo-link',
			'type'     => 'switch',
			'title'    => esc_html__('Logo link', 'schon'),
			'subtitle'    => esc_html__('Logo links to homepage', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

//		array(
//			'id'   => 'info-general-page-options',
//			'type' => 'info',
//			//'raw_html' => true,
//			'class' => 'schon-redux-section-title',
//			'desc' => esc_html__('General Pages Options', 'schon')
//		),



//		array(
//			'id'       => 'show-comments-in-pages',
//			'type'     => 'switch',
//			'title'    => esc_html__('Show Comments for pages', 'schon'),
//			'on'       => esc_html__('Yes', 'schon'),
//			'off'      => esc_html__('No', 'schon'),
//			'default'  => false,
//		),


		array(
			'id'   => 'info-general-customizations',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('General Customizations', 'schon')
		),

		array(
			'id'       => 'custom-css',
			'type'     => 'ace_editor',
			'title'    => esc_html__('Custom CSS', 'schon'),
			'subtitle' => esc_html__('Block before &lt;/head&gt;. Paste your CSS code here.', 'schon'),
			'desc'     => esc_html__('Without &lt;style&gt; and &lt;/style&gt;', 'schon'),
			'mode'     => 'css',
			'theme'    => 'monokai',
			'default'  => ""
		),

		array(
			'id'       => 'custom-javascript',
			'type'     => 'ace_editor',
			'title'    => esc_html__('Custom Javascript or Analytics code', 'schon'),
			'subtitle' => esc_html__('Block before &lt;/body&gt;. Paste your Javascript or Analytics code here.', 'schon'),
			'desc'     => esc_html__('Without &lt;script&gt; and &lt;/script&gt;', 'schon'),
			'mode'     => 'javascript',
			'theme'    => 'monokai',
			'default'  => ""
		)

	)
));


//LAYOUT AND SIDEBAR DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Layout and Sidebars', 'schon' ),
	'icon'   => 'el-icon-home',
	'fields' => array(

		array(
			'id'       => 'boxed-layout',
			'type'     => 'switch',
			'title'    => esc_html__('Boxed layout', 'schon'),
			'desc'    => esc_html__('Enable boxed layout for the whole site', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'boxed-background-type',
			'type'     => 'button_set',
			'title'    => esc_html__('Background Type', 'schon'),
			'options' => array(
				'1' => esc_html__('Color', 'schon'),
				'2' => esc_html__('Image', 'schon'),
				'3' => esc_html__('Pattern', 'schon'),
			),
			'default' => '1',
			'required' => array(
				array('boxed-layout','=', true),
			)
		),

		array(
			'id'       => 'body-background-color',
			'type'     => 'color_gradient',
			'title'    => esc_html__('Background Color', 'schon'),
			'subtitle' => esc_html__("If you don't want use a gradient, select two same colors", 'schon'),
			'validate' => 'color',
			'transparent' => false,
			'default'  => array(
				'from' => '#eeeeee',
				'to'   => '#eeeeee',
			),
			'required' => array(
				array('boxed-layout','=', true),
				array('boxed-background-type','=', '1'),
			)
		),

		/*array(
			'id'       => 'body-background-image',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__('Background Image', 'schon'),
			'required' => array(
				array('boxed-layout','=', true),
				array('boxed-background-type','=', '2'),
			)
		),*/

		array(
			'id'       => 'body-background-image',
			'type'     => 'background',
			'title'    => esc_html__('Body Background', 'schon'),
			'background-color' => false,
			'default'  => array(
				'background-repeat' => 'no-repeat',
				'background-size' => 'cover',
				'background-attachment' => 'fixed',
				'background-position' => 'center center',
				'background-color' => '#1e73be',
			),
			'required' => array(
				array('boxed-layout','=', true),
				array('boxed-background-type','=', '2'),
			),
		),

		array(
			'id'       => 'body-background-pattern',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Pattern Background', 'schon' ),
			'options'  => array(
				'pattern_01.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_01.png'
				),
				'pattern_02.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_02.png'
				),
				'pattern_03.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_03.png'
				),
				'pattern_04.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_04.png'
				),
				'pattern_05.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_05.png'
				),
				'pattern_06.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_06.png'
				),
				'pattern_07.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_07.png'
				),
				'pattern_08.png' => array(
					'img' => get_template_directory_uri() . '/admin/options/patterns/pattern_08.png'
				),
			),
			'default'  => 'tree_bark.png',
			'required' => array(
				array('boxed-layout','=', true),
				array('boxed-background-type','=', '3'),
			),
			'class' => 'body-background-pattern'
		),


	/*	array(
			'id'   => 'info-default-layout',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Pages Layout', 'schon')
		),*/

		array(
			'id'       => 'default-layout',
			'type'     => 'image_select',
//			'compiler' => true,
			'title'    => esc_html__( 'Default Layout', 'schon' ),
			'subtitle' => esc_html__( 'Default sidebar alignment for pages.', 'schon' ),
			'options'  => array(
				'1' => array(
					'alt' => 'No Sidebar',
					'img' => get_template_directory_uri() . '/admin/options/img/1col.png'
				),
				'2' => array(
					'alt' => 'Sidebar Right',
					'img' => get_template_directory_uri() . '/admin/options/img/2cr.png'
				),
				'3' => array(
					'alt' => 'Sidebar Left',
					'img' => get_template_directory_uri() . '/admin/options/img/2cl.png'
				),
				'4' => array(
					'alt' => 'Sidebar Collapsed',
					'img' => get_template_directory_uri() . '/admin/options/img/sidebar-collapsed.png'
				)
			),
			'default'  => '1'
		),


		/*array(
			'id'   => 'info-blog-layout',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Blog and Posts Layout ', 'schon')
		),*/

		array(
			'id'       => 'blog-layout',
			'type'     => 'image_select',
//			'compiler' => true,
			'title'    => esc_html__( 'Blog Layout', 'schon' ),
			'subtitle' => esc_html__( 'Sidebar alignment for Archives, categories, articles.', 'schon' ),
			'options'  => array(
				'1' => array(
					'alt' => 'No Sidebar',
					'img' => get_template_directory_uri() . '/admin/options/img/1col.png'
				),
				'2' => array(
					'alt' => 'Sidebar Right',
					'img' => get_template_directory_uri() . '/admin/options/img/2cr.png'
				),
				'3' => array(
					'alt' => 'Sidebar Left',
					'img' => get_template_directory_uri() . '/admin/options/img/2cl.png'
				),
				'4' => array(
					'alt' => 'Sidebar Collapsed',
					'img' => get_template_directory_uri() . '/admin/options/img/sidebar-collapsed.png'
				)
			),
			'default'  => '3'
		),


		/*array(
			'id'   => 'info-woocommerce-layout',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Woocommerce Pages Layout ', 'schon')
		),*/

		array(
			'id'       => 'woocommerce-layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Woocommerce Layout', 'schon' ),
			'subtitle' => esc_html__( 'Sidebar alignment for Shop and Product pages', 'schon' ),
			'options'  => array(
				'1' => array(
					'alt' => 'No Sidebar',
					'img' => get_template_directory_uri() . '/admin/options/img/1col.png'
				),
				'2' => array(
					'alt' => 'Sidebar Right',
					'img' => get_template_directory_uri() . '/admin/options/img/2cr.png'
				),
				'3' => array(
					'alt' => 'Sidebar Left',
					'img' => get_template_directory_uri() . '/admin/options/img/2cl.png'
				),
				'4' => array(
					'alt' => 'Sidebar Collapsed',
					'img' => get_template_directory_uri() . '/admin/options/img/sidebar-collapsed.png'
				)
			),
			'default'  => '3'
		),

		array(
			'id'       => 'woocommerce-single-product-layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Woocommerce Single Product Layout', 'schon' ),
			'subtitle' => esc_html__( 'Sidebar alignment for single products', 'schon' ),
			'options'  => array(
				'1' => array(
					'alt' => 'No Sidebar',
					'img' => get_template_directory_uri() . '/admin/options/img/1col.png'
				),
				'2' => array(
					'alt' => 'Sidebar Right',
					'img' => get_template_directory_uri() . '/admin/options/img/2cr.png'
				),
				'3' => array(
					'alt' => 'Sidebar Left',
					'img' => get_template_directory_uri() . '/admin/options/img/2cl.png'
				),
				'4' => array(
					'alt' => 'Sidebar Collapsed',
					'img' => get_template_directory_uri() . '/admin/options/img/sidebar-collapsed.png'
				)
			),
			'default'  => '1'
		),

	)
));


// HEADER DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Header', 'schon' ),

	'fields' => array(

		array(
			'id'       => 'header_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Header layout', 'schon' ),
			'options'  => array(
				'layout_1' => array(
					'alt' => 'Layout 1',
					'img' => get_template_directory_uri() . '/admin/options/img/headers/header-1.jpg'
				),
				'layout_2' => array(
					'alt' => 'Layout 1',
					'img' =>  get_template_directory_uri() . '/admin/options/img/headers/header-2.jpg'
				)
			),
			'default'  => 'layout_1'
		),

//		array(
//			'id'       => 'primary-menu-align',
//			'type'     => 'button_set',
//			'title'    => esc_html__('Menu align', 'schon'),
//			'options' => array(
//				'1' => esc_html__('Left', 'schon'),
//				'2' => esc_html__('Center', 'schon'),
//				'3' => esc_html__('Right', 'schon'),
//			),
//			'default' => '2'
//		),

		array(
			'id'       => 'header-background-image',
			'type'     => 'background',
			'title'    => esc_html__('Background image for the header', 'schon'),
			'subtitle'     => esc_html__('This option will set a transparent background for the navigation bar and the action bar.', 'schon'),
			'compiler' => array('#masthead'),
			'background-color' => false,
			'default'  => array(
				'background-repeat' => 'no-repeat',
				'background-size' => 'cover',
				'background-attachment' => 'fixed',
				'background-position' => 'center center'
			),
		),

		array(
			'id'       => 'overlapping-header',
			'type'     => 'switch',
			'title'    => esc_html__('Overlapping Header', 'schon'),
			'subtitle'    => esc_html__('Enable the overlapping of the header on page content.', 'schon'),
			'desc'    => esc_html__('This option can be overriden page by page, excpet on woocommerce page (Shop, Cart, Checkout).', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'sticky-header',
			'type'     => 'switch',
			'title'    => esc_html__('Sticky Header', 'schon'),
			'subtitle'    => esc_html__('Attach the header to the top of the page when you scroll down', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-search-primary-navigation',
			'type'     => 'switch',
			'title'    => esc_html__('Show the search icon', 'schon'),
			'subtitle'    => esc_html__('Show the search icon in the primary navigation area', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-wishlist-primary-navigation',
			'type'     => 'switch',
			'title'    => esc_html__('Show the wishlist icon', 'schon'),
			'subtitle'    => esc_html__('Show the wishlist icon in the primary navigation area', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-cart-primary-navigation',
			'type'     => 'switch',
			'title'    => esc_html__('Show the cart icon', 'schon'),
			'subtitle'    => esc_html__('Show the wishlist icon in the primary navigation area', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'   => 'info_actionbar',
			'type' => 'info',
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Action Bar', 'schon')
		),

		array(
			'id'       => 'show-action-bar',
			'type'     => 'switch',
			'title'    => esc_html__('Show Action Bar', 'schon'),
			'subtitle'    => esc_html__('Show Action Bar above the header', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'   => 'info_staticblocks',
			'type' => 'info',
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Static Blocks', 'schon')
		),

		array(
			'id'=>'staticblock-actionbar-left-fields',
			'type' => 'multi_text',
			'title' => __('Actionbar: left', 'schon'),
			'validate' => 'html',
			'subtitle' => __('Static blocks in the actionbar, on the left', 'schon'),
			'desc' => __('You can use html tags and shortcodes here', 'schon'),
			'show_empty' => true,
			'default' => array(
				'Unlimited blocks like this',
				'<i class="fa fa-envelope-o"></i> info@schon.store',
				'[schon_social_icons]'
			)
		),
		array(
			'id'=>'staticblock-actionbar-right-fields',
			'type' => 'multi_text',
			'title' => __('Actionbar: right', 'schon'),
			'validate' => 'html',
			'subtitle' => __('Static blocks in the actionbar, on the right', 'schon'),
			'desc' => __('You can use html tags and shortcodes here', 'schon'),
			'show_empty' => true,
			'default' => array(
				'[schon_loggedinout_menu]',
			)
		),

		array(
			'id'=>'staticblock-right-logo',
			'type' => 'multi_text',
			'title' => __('Primary navigation: near logo', 'schon'),
			'validate' => 'html',
			'subtitle' => __('Static blocks in the primary navigation, on the right of the logo', 'schon'),
			'desc' => __('You can use html tags and shortcodes here', 'schon'),
			'show_empty' => true,
			'default' => array(
				'<i class="fa fa-phone"></i> +1 (555) 333 22 11'
			)
		),

	)
));

// TITLE BAR DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Title bar', 'schon' ),

	'fields' => array(
		array(
			'id'       => 'show-titlebar',
			'title'    => esc_html__('Show title bar', 'schon'),
			'type'     => 'switch',
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'page-title-bar-alignment',
			'type'     => 'button_set',
			'title'    => esc_html__('Page Title Bar Alignment', 'schon'),
			//Must provide key => value pairs for radio options
			'options'  => array(
				'text-left' => 'Left',
				'text-center' => 'Center',
				'text-right' => 'Right',
			),
			'default' => 'text-center'
		),

		array(
			'id'       => 'titlebar-default-image',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__('Default background', 'schon'),
			'default'  => array(
				'url'=>''
			)
		),

		array(
			'id'       => 'titlebar-padding',
			'type'     => 'text',
			'title'    => __('Top padding', 'schon'),
			'subtitle' => __('Padding above the title, subtitle and breadcrumbs in title bar', 'schon'),
			'desc'     => __('Insert just numeric int value, without px', 'schon'),
			'validate' => 'numeric',
			'default'  => 70,
			'compiler' => true
		),

		array(
			'id'       => 'titlebar-padding-bottom',
			'type'     => 'text',
			'title'    => __('Bottom padding', 'schon'),
			'subtitle' => __('Padding below the title, subtitle and breadcrumbs in title bar', 'schon'),
			'desc'     => __('Insert just numeric int value, without px', 'schon'),
			'validate' => 'numeric',
			'default'  => 70,
			'compiler' => true
		),

		array(
			'id'       => 'titlebar_color_style',
			'type'     => 'button_set',
			'title'    => esc_html__('Style', 'schon'),
			'subtitle' => esc_html__('Style of the overlay and text in the title bar', 'schon'),
			'desc' => esc_html__('"Dark" and "Light" are refered to the overlay color, the color of the text will be contrasted automatically.', 'schon'),
			'options' => array(
				'dark' => esc_html__('Dark', 'schon'),
				'light' => esc_html__('Light', 'schon'),
			),
			'default' => 'dark'
		),

		array(
			'id'       => 'show-breadcrumb',
			'title'    => esc_html__('Show the breadcrumbs', 'schon'),
			'type'     => 'switch',
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),
	),
));

//FOOTER DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Footer', 'schon' ),
	'icon'   => 'el-icon-home',
	'fields' => array(

		array(
			'id'       => 'footer-margin-top',
			'type'     => 'text',
			'title'    => __('Footer margin top', 'schon'),
			'compiler' => true,
			'desc'     => __('Insert just numeric int value, without px', 'schon'),
			'validate' => 'numeric',
			'default'  => 50,
		),

		array(
			'id'       => 'footer-widget-row-1',
			'type'     => 'image_select',
//			'compiler' => true,
			'title'    => esc_html__( 'Widget Column Layout - Row 1', 'schon' ),
			'desc'    => esc_html__( 'The columns layout of the first row of widgets', 'schon' ),
			'options'  => array(
				'6-6' => array(
					'alt' => '6-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-6.png'
				),
				'4-4-4' => array(
					'alt' => '4-4-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-4-4.png'
				),
				'3-3-3-3' => array(
					'alt' => '3-3-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-3-3.png'
				),
				'2-2-2-2-2-2' => array(
					'alt' => '2-2-2-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-2-2-2.png'
				),
				'4-8' => array(
					'alt' => '4-8',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-8.png'
				),
				'8-4' => array(
					'alt' => '8-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/8-4.png'
				),
				'3-3-6' => array(
					'alt' => '3-3-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-6.png'
				),
				'6-3-3' => array(
					'alt' => '6-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-3-3.png'
				),
				'3-6-3' => array(
					'alt' => '3-6-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-6-3.png'
				),
				'3-4-5' => array(
					'alt' => '3-4-5',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-4-5.png'
				),
				'5-4-3' => array(
					'alt' => '5-4-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/5-4-3.png'
				),
				'2-2-2-6' => array(
					'alt' => '2-2-2-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-6.png'
				),
				'2-2-4-4' => array(
					'alt' => '2-2-4-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-4-4.png'
				),
				'2-4-2-4' => array(
					'alt' => '2-4-2-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-4-2-4.png'
				),
				'2-4-4-2' => array(
					'alt' => '2-4-4-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-4-4-2.png'
				),
				'4-2-4-2' => array(
					'alt' => '4-2-4-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-2-4-2.png'
				),
				'4-4-2-2' => array(
					'alt' => '4-4-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-4-2-2.png'
				),
				'6-2-2-2' => array(
					'alt' => '6-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-2-2-2.png'
				),
				'2-2-2-3-3' => array(
					'alt' => '2-2-2-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-3-3.png'
				),
				'2-3-2-3-2' => array(
					'alt' => '2-3-2-3-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-3-2-3-2.png'
				),
				'3-3-2-2-2' => array(
					'alt' => '3-3-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-2-2-2.png'
				),
				'3-2-2-2-3' => array(
					'alt' => '3-2-2-2-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-2-2-2-3.png'
				),

			),
			'default'  => '3-3-3-3'
		),

		array(
			'id'       => 'footer-widget-row-2',
			'type'     => 'image_select',
//			'compiler' => true,
			'title'    => esc_html__( 'Widget Column Layout - Row 2', 'schon' ),
			'desc'    => esc_html__( 'The columns layout of the second row of widgets', 'schon' ),
			'options'  => array(
				'6-6' => array(
					'alt' => '6-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-6.png'
				),
				'4-4-4' => array(
					'alt' => '4-4-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-4-4.png'
				),
				'3-3-3-3' => array(
					'alt' => '3-3-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-3-3.png'
				),
				'2-2-2-2-2-2' => array(
					'alt' => '2-2-2-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-2-2-2.png'
				),
				'4-8' => array(
					'alt' => '4-8',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-8.png'
				),
				'8-4' => array(
					'alt' => '8-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/8-4.png'
				),
				'3-3-6' => array(
					'alt' => '3-3-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-6.png'
				),
				'6-3-3' => array(
					'alt' => '6-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-3-3.png'
				),
				'3-6-3' => array(
					'alt' => '3-6-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-6-3.png'
				),
				'3-4-5' => array(
					'alt' => '3-4-5',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-4-5.png'
				),
				'5-4-3' => array(
					'alt' => '5-4-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/5-4-3.png'
				),
				'2-2-2-6' => array(
					'alt' => '2-2-2-6',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-6.png'
				),
				'2-2-4-4' => array(
					'alt' => '2-2-4-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-4-4.png'
				),
				'2-4-2-4' => array(
					'alt' => '2-4-2-4',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-4-2-4.png'
				),
				'2-4-4-2' => array(
					'alt' => '2-4-4-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-4-4-2.png'
				),
				'4-2-4-2' => array(
					'alt' => '4-2-4-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-2-4-2.png'
				),
				'4-4-2-2' => array(
					'alt' => '4-4-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/4-4-2-2.png'
				),
				'6-2-2-2' => array(
					'alt' => '6-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/6-2-2-2.png'
				),
				'2-2-2-3-3' => array(
					'alt' => '2-2-2-3-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-2-2-3-3.png'
				),
				'2-3-2-3-2' => array(
					'alt' => '2-3-2-3-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/2-3-2-3-2.png'
				),
				'3-3-2-2-2' => array(
					'alt' => '3-3-2-2-2',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-3-2-2-2.png'
				),
				'3-2-2-2-3' => array(
					'alt' => '3-2-2-2-3',
					'img' => get_template_directory_uri() . '/admin/options/img/footer/3-2-2-2-3.png'
				),

			),
			'default'  => '3-2-2-2-3'
		),

		array(
			'id'       => 'footer-widgets-accordion-enabled',
			'type'     => 'switch',
			'title'    => esc_html__('Enable accordion in responsive view', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'copy-social-bar',
			'type'     => 'switch',
			'title'    => esc_html__('Show Copyright section', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'               => 'footer-copyright-left',
			'type'             => 'editor',
			'title'            => esc_html__('Copyright section: left', 'schon'),
			'subtitle'         => esc_html__('The content of the copyright area on the left side.', 'schon'),
			'default'          => "© <strong>sh&ouml;n</strong> - All Right reserved.",
			'args'   => array(
				'teeny'            => true,
				'textarea_rows'    => 8,
				'wpautop' => false
			)
		),
		array(
			'id'               => 'footer-copyright-center',
			'type'             => 'editor',
			'title'            => esc_html__('Copyright section: center', 'schon'),
			'subtitle'         => esc_html__('The content of the copyright area aligned to center.', 'schon'),
			'default'          => '',
			'args'   => array(
				'teeny'            => true,
				'textarea_rows'    => 8,
				'wpautop' => false
			)
		),
		array(
			'id'               => 'footer-copyright-right',
			'type'             => 'editor',
			'title'            => esc_html__('Copyright section: right', 'schon'),
			'subtitle'         => esc_html__('The content of the copyright area on the right side.', 'schon'),
			'default'          => 'Developed by <a href="http://themeforest.net/user/webbaku">Webbaku</a>',
			'args'   => array(
				'teeny'            => true,
				'textarea_rows'    => 8,
				'wpautop' => false
			)
		),


	)
));


//Woocommerce Declaration
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Woocommerce', 'schon' ),
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Woocommerce General', 'schon' ),
	'heading' => 'General',
	'subsection' => true,
	'fields' => array(

		array(
			'id'       => 'show-shop-steps',
			'type'     => 'switch',
			'title'    => esc_html__('Show the shopping steps', 'schon'),
			'subtitle'    => esc_html__('Show the shopping steps process in the pages Cart, Checkout and Order Received.', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'minicart_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Minicart layout', 'schon' ),
			'options'  => array(
				'layout_1' => array(
					'alt' => 'Layout 1',
					'img' => get_template_directory_uri() . '/admin/options/img/minicart-1.jpg'
				),
				'layout_2' => array(
					'alt' => 'Layout 2',
					'img' => get_template_directory_uri() . '/admin/options/img/minicart-2.jpg'
				)
			),
			'default'  => 'layout_1',
			'class' => 'image_select_1-2'
		),

		array(
			'id'   => 'info-woocommerce-catalog-mode',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Catalog Mode', 'schon')
		),

		array(
			'id'       => 'catalog-mode-enabled',
			'type'     => 'switch',
			'title'    => esc_html__('Enable the catalog mode', 'schon'),
			'subtitle'    => esc_html__('This will hide all "Add to cart" buttons and the pages "Cart" and "Checkout".', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'shopping-price-disabled',
			'type'     => 'switch',
			'title'    => esc_html__('Hide all the prices', 'schon'),
			'subtitle'    => esc_html__('This will hide the prices of all the products', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
			'required'  => array(
				array('catalog-mode-enabled','=',true)
			)
		),
	)
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Loop', 'schon' ),
	'subsection' => true,
	'fields' => array(

		array(
			'id'       => 'woocommerce-filters-sidebar-enabled',
			'type'     => 'switch',
			'title'    => esc_html__('Enable the filters accordion', 'schon'),
			'subtitle'    => esc_html__('You can insert additional filters in the sidebar "Woocommerce Filters Accordion"', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'woocommerce-number-of-columns',
			'type'     => 'button_set',
			'title'    => esc_html__('Number of columns', 'schon'),
			'options' => array(
				'2' => esc_html__('2', 'schon'),
				'3' => esc_html__('3', 'schon'),
				'4' => esc_html__('4', 'schon'),
				'5' => esc_html__('5', 'schon'),
			),
			'default' => '3',
		),

		array(
			'id'       => 'woocommerce-number-of-products',
			'type'     => 'text',
			'title'    => __('Number of products per page', 'schon'),
			'validate' => 'numeric',
			'default'  => 9,
		),

		array(
			'id'       => 'show-quickview-button-loop',
			'type'     => 'switch',
			'title'    => esc_html__('Show the QuickView button in the loop secion', 'schon'),
			'subtitle'    => esc_html__('Show the quickview button on every product in the loop.', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-wishlist-button-loop',
			'type'     => 'switch',
			'title'    => esc_html__('Show the Wishlist button in the loop secion', 'schon'),
			'subtitle'    => esc_html__('Show the wishlist button on every product in the loop.', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-compare-button-loop',
			'type'     => 'switch',
			'title'    => esc_html__('Show the compare button in the loop secion', 'schon'),
			'subtitle'    => esc_html__('Show the compare button on every product in the loop.', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

		array(
			'id'       => 'product-align-text',
			'type'     => 'button_set',
			'title'    => esc_html__('Align products content in grid view', 'schon'),
			'options' => array(
				'text-left' => esc_html__('Left', 'schon'),
				'text-center' => esc_html__('Center', 'schon'),
			),
			'default' => 'text-left',
		),

	)
));

Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Single Product', 'schon' ),
	'subsection' => true,
	//'icon'   => 'el-shopping-cart',
	'fields' => array(

		array(
			'id'       => 'single_product_details_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Details Layout', 'schon' ),
			'options'  => array(
				'layout_1' => array(
					'img' => get_template_directory_uri() . '/admin/options/img/summary-product-1.jpg'
				),
				'layout_2' => array(
					'img' => get_template_directory_uri() . '/admin/options/img/summary-product-2.jpg'
				),
			),
			'default'  => 'layout_2',
			'class' => 'image_select_1-2'
		),

		array(
			'id'       => 'single_product_background_layout',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Background Layout', 'schon' ),
			'options'  => array(
				'layout_1' => array(
					'img' => get_template_directory_uri() . '/admin/options/img/product-layout-1.jpg'
				),
				'layout_2' => array(
					'img' => get_template_directory_uri() . '/admin/options/img/product-layout-2.jpg'
				),
				'layout_3' => array(
					'img' => get_template_directory_uri() . '/admin/options/img/product-layout-3.jpg'
				),
			),
			'default'  => 'layout_1',
			'class' => 'image_select_1-3'
		),

		array(
			'id'       => 'show-titlebar-single-product',
			'type'     => 'switch',
			'title'    => esc_html__('Show title bar', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => false,
		),

	)
));

//Blog Declaration
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Blog', 'schon' ),
	'fields' => array(
		array(
			'id'   => 'info-blog-loop',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('List', 'schon')
		),

		array(
			'id'       => 'blog-date-as-badge',
			'type'     => 'switch',
			'title'    => esc_html__('Show date as badge', 'schon'),
			'subtitle'    => esc_html__('Show the date as a badge in the left of the article instead of as a string', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'blog-listing-layout',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Blog listing layout', 'schon' ),
			'options'  => array(
				'layout-1' => esc_html('Standard',' schon'),
				'layout-2' => esc_html('List',' schon'),
				'layout-3' => esc_html('Grid',' schon')
			),
			'default'  => 'layout-1'
		),

		array(
			'id'   => 'info-blog-single',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Single Post', 'schon')
		),

		array(
			'id'       => 'blog-show-author-box',
			'type'     => 'switch',
			'title'    => esc_html__('Show author box', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'blog-show-post-navigation',
			'type'     => 'switch',
			'title'    => esc_html__('Show post navigation', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

		array(
			'id'       => 'show-titlebar-single-post',
			'type'     => 'switch',
			'title'    => esc_html__('Show title bar', 'schon'),
			'desc'    => esc_html__('Show title bar in single posts', 'schon'),
			'on'       => esc_html__('Yes', 'schon'),
			'off'      => esc_html__('No', 'schon'),
			'default'  => true,
		),

	)
));

// Typography DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Typography', 'schon' ),

));

Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'General', 'schon' ),
	'icon'   => false,
	'subsection' => true,

	'fields' => array(

		array(
			'id'          => 'typography-primary-text',
			'type'        => 'typography',
			'title'       => esc_html__('Primary Font Family', 'schon'),
			'subtitle'    => esc_html__("Headings, titles, top header (actionbar) fields, project filters, tag cloud items", 'schon'),
			'google'      => true,
			'font-backup' => true,
			'compiler'      => true,
			'font-size' => false,
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-style' => false,
			'font-weight' => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'all_styles' => true,
			'default'     => array(
				'font-family' => 'Montserrat',
				'google'      => true
			),
		),

		array(
			'id'          => 'typography-text',
			'type'        => 'typography',
			'title'       => esc_html__('Secondary font family', 'schon'),
			'subtitle'    => esc_html__("All remaining text (general body font)", 'schon'),
			'google'      => true,
			'font-backup' => true,
			'compiler'      => array('.sampe-typography-text'),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-style' => false,
			'font-weight' => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'all_styles' => true,
			'default'     => array(
				'font-family' => 'Source Sans Pro',
				'google'      => true,
				'font-size'   => '14px',
			),
		),


		array(
			'id'          => 'typography-main-menu',
			'type'        => 'typography',
			'title'       => esc_html__('Main Menu', 'schon'),
			'google'      => true,
			'font-backup' => true,
			'compiler'      => array(
				'#primary-menu a',
				'#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a',
				'#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a'
			),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'subsets'     => false,
			'font-weight' => false,
			'font-style'  => false,
			'line-height' => false,
			'color'       => false,
			'default'     => array(
				'font-family' => 'Montserrat',
				'google'      => true,
				'font-size'   => '14px',
			),
		),


/*
		array(
			'id'          => 'typography-page-title-bar-title',
			'type'        => 'typography',
			'title'       => esc_html__('Page Title Bar | Title', 'schon'),
			'google'      => false,
			'font-backup' => false,
			'font-family' => false,
			'compiler'      => array('.page-title-bar-container .entry-title'),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-weight' => false,
			'font-style'  => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'default'     => array(
				'google'      => true,
				'font-size'   => '44px',
			),
		),

		array(
			'id'          => 'typography-page-title-bar-subtitle',
			'type'        => 'typography',
			'title'       => esc_html__('Page Title Bar | Subtitle', 'schon'),
			'google'      => false,
			'font-backup' => false,
			'font-family' => false,
			'compiler'      => array('.page-title-bar-container .entry-subtitle'),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-weight' => false,
			'font-style'  => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'default'     => array(
				'font-size'   => '18px',
			),
		),
*/

		array(
			'id'          => 'typography-footer-heading',
			'type'        => 'typography',
			'title'       => esc_html__('Footer Widgets Title', 'schon'),
			'google'      => false,
			'font-backup' => false,
			'font-family' => false,
			'compiler'      => array('.footer-widgets-row .widget-title'),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-weight' => false,
			'font-style'  => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'default'     => array(
				'font-size'   => '16px',
			),
		),

		array(
			'id'          => 'typography-sidebar-heading',
			'type'        => 'typography',
			'title'       => esc_html__('Sidebar Widgets Title', 'schon'),
			'google'      => false,
			'font-backup' => false,
			'font-family' => false,
			'compiler'      => array('#secondary .widget-title'),
			'units'       =>'px',
			'text-align'  => false,
			'text-transform' => false,
			'font-weight' => false,
			'font-style'  => false,
			'subsets'     => false,
			'line-height' => false,
			'color'       => false,
			'default'     => array(
				'font-size'   => '22px',
			),
		),

	)
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Content Headings', 'schon' ),
	'icon'   => false,
	'subsection' => true,

	'fields' => array(
		array(
			'id' => 'typography-h1-size',
			'type' => 'typography',
			'title' => esc_html__('h1, .h1', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '40px',
			),
			'compiler' => array('h1','.h1')
		),

		array(
			'id' => 'typography-h2-size',
			'type' => 'typography',
			'title' => esc_html__('h2, .h2', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '28px',
			),
			'compiler' => array('h2','.h2')
		),

		array(
			'id' => 'typography-h3-size',
			'type' => 'typography',
			'title' => esc_html__('h3, .h3', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '22px',
			),
			'compiler' => array('h3','.h3')
		),

		array(
			'id' => 'typography-h4-size',
			'type' => 'typography',
			'title' => esc_html__('h4, .h4', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '16px',
			),
			'compiler' => array('h4','.h4')
		),

		array(
			'id' => 'typography-h5-size',
			'type' => 'typography',
			'title' => esc_html__('h5, .h5', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '14px',
			),
			'compiler' => array('h5','.h5')
		),

		array(
			'id' => 'typography-h6-size',
			'type' => 'typography',
			'title' => esc_html__('h6, .h6', 'schon'),
			'google' => false,
			'font-backup' => false,
			'units' =>'px',
			'text-align' => false,
			'text-transform' => false,
			'subsets' => false,
			'line-height' => false,
			'color' => false,
			'letter-spacing' => false,
			'font-family' => false,
			'font-weight' => false,
			'font-style'  => false,
			'font-size' => true,
			'preview' => false,
			'default'     => array(
				'font-size'   => '12px',
			),
			'compiler' => array('h6','.h6')
		),

	)
));


// SOCIAL LINKS DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Social links', 'schon' ),
	'fields' => array(

		array(
			'id'       => 'social-facebook',
			'type'     => 'text',
			'title'    => esc_html__('Facebook', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-twitter',
			'type'     => 'text',
			'title'    => esc_html__('Twitter', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-instagram',
			'type'     => 'text',
			'title'    => esc_html__('Instagram', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-youtube',
			'type'     => 'text',
			'title'    => esc_html__('Youtube', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-linkedin',
			'type'     => 'text',
			'title'    => esc_html__('Linkedin', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-dribbble',
			'type'     => 'text',
			'title'    => esc_html__('Dribbble', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-flickr',
			'type'     => 'text',
			'title'    => esc_html__('Flickr', 'schon'),
			'default'  => '#',
		),

		array(
			'id'       => 'social-pinterest',
			'type'     => 'text',
			'title'    => esc_html__('Pinterest', 'schon'),
			'default'  => '#',
		),

	)
));


// COLORS DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Colors', 'schon' ),
));

Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'General', 'schon' ),
	'id' => 'color-dclaration-general',
	'icon'   => false,
	'subsection' => true,
	'fields' => array(

		array(
			'id'       => 'page-background-color',
			'type'     => 'color',
			'title'    => esc_html__('Content background color', 'schon'),
			'subtitle' => esc_html__('If is not set boxed style, this is background of the whole pages', 'schon'),
			'desc' => esc_html__('Default:', 'schon'). " <i>#ffffff</i>",
			'compiler' => array( 'background-color' => '#page' ),
			'transparent' => false,
			'default' => '#ffffff'
		),

		array(
			'id'       => 'general-color',
			'type'     => 'color',
			'title'    => esc_html__('General text color', 'schon'),
			'desc' => esc_html__('Default:', 'schon'). " <i>#757575</i>",
			'compiler' => array(
				'color' => '
					body,
					a,
					#actionbar a:hover,
					#actionbar a:focus,
					#actionbar a:active,
					#actionbar-menu a:hover,
					#actionbar-menu a:focus,
					#actionbar-menu a:active,
					.search-fullpage-form-wrapper .submit-field,
					.close-search-fullpage-trigger,
					.mini-cart-header .cart_list .mini_cart_item_quantity,
					.mini-cart-header .cart_list .mini_cart_item_name,
					.mini-cart-header .total .subtotal-label,
					#page ul.products li.product .add-to-cart-buttons-container a,
					#page ul.products li.product .add-to-cart-buttons-container a,
					#page ul.products li.product .add-to-cart-buttons-container a,
					.yith-woocompare-widget ul.products-list li a.remove:before,
					.woocommerce div.product form.cart .variations label,
					.woocommerce div.product .woocommerce-tabs ul.tabs li a,
					nav.schon-pagination ul li a,
					nav.schon-pagination ul li span,
					.woocommerce nav.woocommerce-pagination ul li a,
					.woocommerce nav.woocommerce-pagination ul li span,
					nav.schon-pagination ul li a:hover,
					nav.schon-pagination ul li span.current,
					.woocommerce nav.woocommerce-pagination ul li a:hover,
					.woocommerce nav.woocommerce-pagination ul li span.curren,
					.woocommerce div.product form.cart .variations label .swatchtitlelabel,
					.woocommerce div.product form.cart .variations .label .swatchtitlelabel,
					#page ul.products li.product h3,
					.schon-title-wrap .schon-title-subtitle',
				'background-color' => '
					.mini-cart-header .mini-cart-icon a span,
					#page .widget_price_filter .ui-slider .ui-slider-range,
					.images .thumbnails.owl-theme .owl-controls .owl-page,
					.images .thumbnails.owl-theme .owl-controls .owl-buttons div,
					.blog-post-date-badge .entry-header .entry-meta span.posted-on a,
					.schon-team-member .socials-wrap a',
				'border-color' => '
					#page .widget_price_filter .ui-slider .ui-slider-handle,
					nav.schon-pagination ul li a,
					nav.schon-pagination ul li span,
					.woocommerce nav.woocommerce-pagination ul li a,
					.woocommerce nav.woocommerce-pagination ul li span,
					.schon-owl-theme .owl-theme .owl-buttons div,
					.widget .tagcloud a'
			),
			'transparent' => false,
			'default' => '#757575'
		),

		array(
			'id'       => 'light-background-color',
			'type'     => 'color',
			'title'    => esc_html__('Light background color', 'schon'),
			'desc' => esc_html__('Default:', 'schon'). " <i>#f5f5f5</i>",
			'compiler' => array( 'background-color' => '
				#search-fullpage-wrap,
				.search-fullpage-form-wrapper .search-field,
				#footer-widgets-row-1,
				#footer-widgets-row-2,
				#page-title-bar,
				.authorbox,
				.woocommerce.single-product-background-layout_2 div.product .single-product-header,
				.woocommerce.single-product-background-layout_3 div.product .single-product-header,
				.woocommerce.single-product-background-layout_1 div.product div.summary .summary-content,
				.woocommerce.single-product-background-layout_3 div.product .single-product-header:before,
				.woocommerce-checkout .col-2 #order_review_heading,
				.woocommerce-checkout .col-2 .woocommerce-checkout-review-order-table,
				.woocommerce-checkout .col-2 .woocommerce-checkout-review-order h3,
				.woocommerce-checkout .col-2 .wc_payment_methods.payment_methods,
				#yith-quick-view-modal div.product div.summary,
				.schon-owl-theme .owl-theme.navigation-top .owl-controls .owl-buttons,
				.schon-owl-theme .owl-theme.navigation-bottom .owl-controls .owl-buttons,
				.woocommerce #review_form #respond textarea,
				#add_payment_method table.cart .quantity input,
				.woocommerce-cart table.cart .quantity input,
				.woocommerce-checkout table.cart .quantity input,
				#shon-shop_active-filters,
				#schon-shop_filters-accordion-inner' ),
			'transparent' => false,
			'default' => '#f5f5f5'
		),

		array(
			'id'       => 'primary-color',
			'type'     => 'color',
			'title'    => esc_html__('Primary color', 'schon'),
			'desc' => esc_html__('Default:', 'schon'). " <i>#ff6060</i>",
			'compiler' => array(
				'background-color' => '.btn-primary,
					.btn-primary.disabled:hover,
					.btn-primary.disabled:focus,
					.btn-primary.disabled:active,
					.btn-primary.disabled:active:hover,
					.btn-primary.disabled:active:focus,
					.btn-primary[disabled]:hover,
					.btn-primary[disabled]:focus,
					.btn-primary[disabled]:active,
					.btn-primary[disabled]:active:hover,
					.btn-primary[disabled]:active:focus,
					.btn-primary.btn-ghost:active:hover,
					.btn-primary.btn-ghost:active:focus,
					.schon-owl-theme .owl-theme.navigation-top .owl-controls .owl-buttons div:hover,
					.schon-owl-theme .owl-theme.navigation-bottom .owl-controls .owl-buttons div:hover',
				'border-color' => '.widget .tagcloud a:hover,
					.widget .tagcloud a:focus,
					.btn-primary.btn-ghost,
					.btn-primary.btn-ghost.disabled:hover,
					.btn-primary.btn-ghost.disabled:focus,
					.btn-primary.btn-ghost.disabled:active,
					.btn-primary.btn-ghost.disabled:active:hover,
					.btn-primary.btn-ghost.disabled:active:focus,
					.btn-primary.btn-ghost[disabled]:hover,
					.btn-primary.btn-ghost[disabled]:focus,
					.btn-primary.btn-ghost[disabled]:active,
					.btn-primary.btn-ghost[disabled]:active:hover,
					.btn-primary.btn-ghost[disabled]:active:focus,
					.btn-primary.btn-ghost:hover,
					.btn-primary.btn-ghost:focus,
					.btn-primary.btn-ghost:active,
					.btn-primary.btn-ghost:active:hover,
					.btn-primary.btn-ghost:active:focus',
				'color' => 'h1 a:hover,
					.h1 a:hover,
					h2 a:hover,
					.h2 a:hover,
					h3 a:hover,
					.h3 a:hover,
					h4 a:hover,
					.h4 a:hover,
					.h1 a:active,
					h2 a:active,
					.h2 a:active,
					h3 a:active,
					.h3 a:active,
					h4 a:active,
					.h4 a:active,
					.h1 a:focus,
					h2 a:focus,
					.h2 a:focus,
					h3 a:focus,
					.h3 a:focus,
					h4 a:focus,
					.h4 a:focus,
					a:hover,
					a:focus,
					a:active,
					a.hover,
					a.focus,
					a.active,
					#page ul.products li.product .add-to-cart-buttons-container a:hover,
					#page ul.products li.product .add-to-cart-buttons-container a:focus,
					#page ul.products li.product .add-to-cart-buttons-container a:active,
					#page ul.products li.product .add-to-cart-buttons-container a.added,
					#page ul.products li.product .gridlist-buttonwrap a.added i,
					#page ul.products li.product h3:hover,
					ul.products.list li.product .gridlist-buttonwrap a.link:hover i,
					#breadcrumb-wrap li a:hover,
					.btn-primary.btn-ghost,
					.btn-primary.btn-ghost.disabled:hover,
					.btn-primary.btn-ghost.disabled:focus,
					.btn-primary.btn-ghost.disabled:active,
					.btn-primary.btn-ghost.disabled:active:hover,
					.btn-primary.btn-ghost.disabled:active:focus,
					.btn-primary.btn-ghost[disabled]:hover,
					.btn-primary.btn-ghost[disabled]:focus,
					.btn-primary.btn-ghost[disabled]:active,
					.btn-primary.btn-ghost[disabled]:active:hover,
					.btn-primary.btn-ghost[disabled]:active:focus,
					#page ul.products li.product .add-to-cart-buttons-container .add-to-wishlist.wishlist-added,
					#page ul.products li.product .add-to-cart-buttons-container .add-to-compare.compare-added,
					#page ul.products li.product .add-to-cart-buttons-container .add-to-compare.added:after',
			),
			'transparent' => false,
			'default' => '#ff6060'
		),

		array(
			'id'       => 'secondary-color',
			'type'     => 'color',
			'title'    => esc_html__('Secondary color', 'schon'),
			'desc' => esc_html__('Default:', 'schon'). " <i>#2e2e2e</i>",
			'compiler' => array(
				'background-color' => '
					#actionbar,
					.mini-cart-header .mini-cart-icon a span,
					.btn-primary-2,
					.btn-primary-2.disabled:hover,
					.btn-primary-2.disabled:focus,
					.btn-primary-2.disabled:active,
					.btn-primary-2.disabled:active:hover,
					.btn-primary-2.disabled:active:focus,
					.btn-primary-2[disabled]:hover,
					.btn-primary-2[disabled]:focus,
					.btn-primary-2[disabled]:active,
					.btn-primary-2[disabled]:active:hover,
					.btn-primary-2[disabled]:active:focus,
					.btn-primary-2.btn-ghost:hover,
					.btn-primary-2.btn-ghost:focus,
					.btn-primary-2.btn-ghost:active,
					.btn-primary-2.btn-ghost:active:hover,
					.btn-primary-2.btn-ghost:active:focus',
				'border-color' => '
				.btn-primary-2.btn-ghost,
					.btn-primary-2.btn-ghost.disabled:hover,
					.btn-primary-2.btn-ghost.disabled:focus,
					.btn-primary-2.btn-ghost.disabled:active,
					.btn-primary-2.btn-ghost.disabled:active:hover,
					.btn-primary-2.btn-ghost.disabled:active:focus,
					.btn-primary-2.btn-ghost[disabled]:hover,
					.btn-primary-2.btn-ghost[disabled]:focus,
					.btn-primary-2.btn-ghost[disabled]:active,
					.btn-primary-2.btn-ghost[disabled]:active:hover,
					.btn-primary-2.btn-ghost[disabled]:active:focus,
					.btn-primary-2.btn-ghost:hover,
					.btn-primary-2.btn-ghost:focus,
					.btn-primary-2.btn-ghost:active,
					.btn-primary-2.btn-ghost:active:hover,
					.btn-primary-2.btn-ghost:active:focus',
				'color' => '
					h1,
					.h1,
					h2,
					.h2,
					h3,
					.h3,
					h4,
					.h4,
					h5,
					.h5,
					h6,
					.h6,
					h1 a,
					.h1 a,
					h2 a,
					.h2 a,
					h3 a,
					.h3 a,
					h4 a,
					.h4 a,
					h5 a,
					.h5 a,
					h6 a,
					.h6 a,
					.schon-featured-info-title,
					.search-fullpage-form-wrapper .submit-field:hover,
					.search-fullpage-form-wrapper .submit-field:focus,
					.search-fullpage-form-wrapper .submit-field:active,
					.close-search-fullpage-trigger:hover,
					.close-search-fullpage-trigger:focus,
					.close-search-fullpage-trigger:active,
					#breadcrumb-wrap li,
					#breadcrumb-wrap li a,
					.widget .widget-title,
					#page ul.product_list_widget li .amount,
					.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
					.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover,
					#schon-shop-steps-wrapper .step-number,
					#schon-shop-steps-wrapper .step-label,
					.schon-owl-theme .owl-theme .owl-buttons div:hover,
					#page .product .price,
					#yith-quick-view-modal .product .price,
					.mini-cart-header .cart_list .mini_cart_item_price,
					.mini-cart-header .total .subtotal-value,
					.btn-primary-2.btn-ghost,
					.btn-primary-2.btn-ghost.disabled:hover,
					.btn-primary-2.btn-ghost.disabled:focus,
					.btn-primary-2.btn-ghost.disabled:active,
					.btn-primary-2.btn-ghost.disabled:active:hover,
					.btn-primary-2.btn-ghost.disabled:active:focus,
					.btn-primary-2.btn-ghost[disabled]:hover,
					.btn-primary-2.btn-ghost[disabled]:focus,
					.btn-primary-2.btn-ghost[disabled]:active,
					.btn-primary-2.btn-ghost[disabled]:active:hover,
					.btn-primary-2.btn-ghost[disabled]:active:focus'
			),
			'transparent' => false,
			'default' => '#2e2e2e'
		),

	),
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Header', 'schon' ),
	'icon'   => false,
	'subsection' => true,
	'fields' => array(

		array(
			'id'       => 'header-background',
			'type'     => 'color',
			'title'    => esc_html__('Header Background', 'schon'),
			'compiler' => array('background-color' => '#header-wrap, #primary-navigation'),
			'default' => '#fff'
		),


		array(
			'id'       => 'main-menu-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__('Menu primary color', 'schon'),
			'compiler' => true,
			'active'   => false,
			'visited'  => false,
			'default'  => array(
				'regular'  => '#2e2e2e',
				'hover'    => '#ff6060',
			)
		),

		array(
			'id'       => 'headings-megamenu-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__('Megamenu headings color', 'schon'),
			'compiler' => true,
			'active'   => false,
			'visited'  => false,
			'default'  => array(
				'regular'  => '#2e2e2e',
				'hover'    => '#ff6060',
			)
		),


		array(
			'id'   => 'info-actionbar-colors',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Actionbar', 'schon')
		),

		array(
			'id'       => 'actionbar-background',
			'type'     => 'color',
			'title'    => esc_html__('Background color', 'schon'),
			'compiler' => array('background-color' => '#actionbar'),
			'default' => '#2e2e2e'
		),

		array(
			'id'       => 'actionbar-text-color',
			'type'     => 'link_color',
			'title'    => esc_html__('Text/Links color', 'schon'),
			'compiler' => true,
			'visited' => false,
			'active' => false,
			'default'  => array(
				'regular'  => '#ffffff',
				'hover'    => '#ff6060',
			)
		),

		array(
			'id'       => 'actionbar-line-separator',
			'type'     => 'color',
			'title'    => esc_html__('Line separator', 'schon'),
			'subtitle'    => esc_html__('The separation line between the first and second rows of widgets', 'schon'),
			'compiler' => array('border-color' => '#actionbar > .container'),
			'default' => 'transparent'
		),

	),
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Footer', 'schon' ),
	'icon'   => false,
	'subsection' => true,
	'fields' => array(

		array(
			'id'   => 'info-color-widgets-section',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Widgets section', 'schon')
		),
		
		array(
			'id'       => 'background-footer-widgets-row-1',
			'type'     => 'color',
			'title'    => esc_html__('Background widgets - Row 1', 'schon'),
			'compiler' => array('background-color' => '#footer-widgets-row-1'),
			'transparent' => false,
			'default' => '#f5f5f5'
		),

		array(
			'id'       => 'background-footer-widgets-row-2',
			'type'     => 'color',
			'title'    => esc_html__('Background widgets - Row 2', 'schon'),
			'compiler' => array('background-color' => '#footer-widgets-row-2'),
			'transparent' => false,
			'default' => '#f5f5f5'
		),

		array(
			'id'       => 'background-footer-widgets-line-separator',
			'type'     => 'color',
			'title'    => esc_html__('Line separator', 'schon'),
			'subtitle'    => esc_html__('The separation line between the first and second rows of widgets', 'schon'),
			'compiler' => array('border-color' => '#footer-widgets-row-1'),
			'default' => '#e0e0e0'
		),

		array(
			'id'       => 'layout-footer-widgets-line-separator',
			'type'     => 'switch',
			'title'    => esc_html__('Line separator width', 'schon'),
			'subtitle'    => esc_html__('The separation line between the first and second rows of widgets', 'schon'),
			'on'       => esc_html__('Container', 'schon'),
			'off'      => esc_html__('Fullwidth', 'schon'),
			'default'  => true,
			'class' => 'switch-always-enabled'
		),

//		array(
//			'id'       => 'footer-widgets-1-text-color',
//			'type'     => 'color',
//			'title'    => esc_html__('Standard text color - Row 1', 'schon'),
//			'subtitle'    => esc_html__('The standard text color in the first row of widgets.', 'schon'),
//			'compiler' => array('color' => '#footer-widgets-row-1, #footer-widgets-row-1 a'),
//			'transparent' => false,
//			'default' => '#808080'
//		),
//
//		array(
//			'id'       => 'footer-widgets-1-text-featured-color',
//			'type'     => 'color',
//			'title'    => esc_html__('Featured text color - Row 1', 'schon'),
//			'subtitle'    => esc_html__('The featured text color in the first row of widgets.', 'schon'),
//			'desc'    => esc_html__('Featured text, links hover, widget titles, etc.', 'schon'),
//			'compiler' => array('color' => '#footer-widgets-row-1 .widget-title, #footer-widgets-row-1 .schon-featured-info-title, #footer-widgets-row-1 .schon-featured-info-subtitle, #footer-widgets-row-1 a:hover, #footer-widgets-row-1 a:focus, #footer-widgets-row-1 a:active'),
//			'transparent' => false,
//			'default' => '#353535'
//		),
//
//		array(
//			'id'       => 'footer-widgets-2-text-color',
//			'type'     => 'color',
//			'title'    => esc_html__('Standard text color - Row 2', 'schon'),
//			'subtitle'    => esc_html__('The standard text color in the second row of widgets.', 'schon'),
//			'compiler' => array('color' => '#footer-widgets-row-2, #footer-widgets-row-2 a'),
//			'transparent' => false,
//			'default' => '#808080'
//		),
//
//		array(
//			'id'       => 'footer-widgets-2-text-featured-color',
//			'type'     => 'color',
//			'title'    => esc_html__('Featured text color - Row 2', 'schon'),
//			'subtitle'    => esc_html__('The featured text color in the second row of widgets.', 'schon'),
//			'desc'    => esc_html__('Featured text, links hover, widget titles, etc.', 'schon'),
//			'compiler' => array('color' => '#footer-widgets-row-2 .widget-title, #footer-widgets-row-2 .schon-featured-info-title, #footer-widgets-row-2 .schon-featured-info-subtitle, #footer-widgets-row-2 a:hover, #footer-widgets-row-2 a:focus, #footer-widgets-row-2 a:active'),
//			'transparent' => false,
//			'default' => '#353535'
//		),

		array(
			'id'   => 'info-color-siteinfo-section',
			'type' => 'info',
			//'raw_html' => true,
			'class' => 'schon-redux-section-title',
			'desc' => esc_html__('Copyright section', 'schon')
		),

		array(
			'id'       => 'siteinfo-line-separator-color',
			'type'     => 'color',
			'title'    => esc_html__('Line separator', 'schon'),
			'subtitle'    => esc_html__('The separation line above the copyright section', 'schon'),
			'compiler' => array('border-color' => '#site-info'),
			'default' => 'transparent'
		),

		array(
			'id'       => 'siteinfo-line-separator-layout',
			'type'     => 'switch',
			'title'    => esc_html__('Line separator width', 'schon'),
			'subtitle'    => esc_html__('The separation line above the copyright section', 'schon'),
			'on'       => esc_html__('Container', 'schon'),
			'off'      => esc_html__('Fullwidth', 'schon'),
			'default'  => true,
			'class' => 'switch-always-enabled'
		),

		array(
			'id'       => 'siteinfo-background',
			'type'     => 'color',
			'title'    => esc_html__('Site info background', 'schon'),
			'compiler' => array('background-color' => '#site-info'),
			'transparent' => false,
			'default' => '#ffffff'
		),

		array(
			'id'       => 'siteinfo-text-color',
			'type'     => 'link_color',
			'title'    => esc_html__('Text color', 'schon'),
			'compiler' => true,
			'active' => false,
			'visited' => false,
			'default' => array(
				'regular' => '#757575',
				'hover' => '#ff6060'
			)
		),
/*
		array(
			'id'       => 'siteinfo-text-color',
			'type'     => 'color',
			'title'    => esc_html__('Standard text color', 'schon'),
			'subtitle'    => esc_html__('The standard text color in the copyright section.', 'schon'),
			'compiler' => array('color' => '#site-info, #site-info a'),
			'transparent' => false,
			'default' => '#808080'
		),

		array(
			'id'       => 'siteinfo-text-featured-color',
			'type'     => 'color',
			'title'    => esc_html__('Featured text color', 'schon'),
			'subtitle'    => esc_html__('The featured text color in the copyright section.', 'schon'),
			'desc'    => esc_html__('Featured text, links hover, widget titles, etc.', 'schon'),
			'compiler' => array('color' => '#site-info strong, #site-info a:hover, #site-info a:focus, #site-info a:active'),
			'transparent' => false,
			'default' => '#353535'
		),
*/
	),
));
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Sidebar', 'schon' ),
	'icon'   => false,
	'subsection' => true,
	'fields' => array(

		array(
			'id'       => 'sidebar-widget-heading-color',
			'type'     => 'color',
			'title'    => esc_html__('Widget headings color', 'schon'),
			'transparent' => false,
			'compiler' => array('#secondary .widget-title'),
			'default' => '#2e2e2e'
		),

		array(
			'id'       => 'sidebar-text-color',
			'type'     => 'color',
			'title'    => esc_html__('Text color', 'schon'),
			'transparent' => false,
			'compiler' => array('#secondary'),
			'default' => '#757575'
		),

		array(
			'id'       => 'sidebar-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__('Links Color', 'schon'),
			'compiler' => array('#secondary a'),
			'active'   => false,
			'visited'  => false,
			'default' => array(
				'regular' => '#757575',
				'hover' => '#ff6060'
			)
		),
	),
));


// CONTENT DECLARATION
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Content', 'schon' ),

	'fields' => array(
		array(
			'id'       => 'blog-title',
			'type'     => 'text',
			'title'    => esc_html__('Blog Title', 'schon'),
			'subtitle' => esc_html__('Title of blog pages in title page bar.', 'schon'),
			'default' => 'Blog'
		),

		array(
			'id'       => 'blog-subtitle',
			'type'     => 'text',
			'title'    => esc_html__('Blog Subtitle', 'schon'),
			'subtitle' => esc_html__('Subtitle of blog pages in title page bar.', 'schon'),
			'default' => ''
		),

		array(
			'id'       => 'error404-title-bar',
			'type'     => 'text',
			'title'    => esc_html__('Error 404 | Title bar', 'schon'),
			'default' => 'Error 404'
		),

		array(
			'id'       => 'error404-content-title',
			'type'     => 'text',
			'title'    => esc_html__('Error 404 | Content title', 'schon'),
			'default' => 'Page not found'
		),

		array(
			'id'       => 'error404-content-text',
			'type'     => 'text',
			'title'    => esc_html__('Error 404 | Content text', 'schon'),
			'default' => "We are sorry but this page doesn't exist or has been removed."
		),
	),
));





add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

if ( ! function_exists( 'compiler_action' ) ) {
	function compiler_action( $options, $css, $changed_values ) {
//		write_log($options['actionbar-text-color']);
//		write_log($css);
//		write_log($changed_values);
		$filename = dirname( __FILE__ ) . '/../../css/theme-options' . '.css';

		global $wp_filesystem;

		if ( empty( $wp_filesystem ) ) {
			require_once( ABSPATH . '/wp-admin/includes/file.php' );
			WP_Filesystem();
		}

		if ( $wp_filesystem ) {

			if(isset($options['layout-footer-widgets-line-separator']) && $options['layout-footer-widgets-line-separator'] == 1) {
				$css = str_replace('#footer-widgets-row-1{border-color:', '#footer-widgets-row-1>.container{border-color:', $css);
			} else {
				$css = str_replace('#footer-widgets-row-1>.container{border-color:', '#footer-widgets-row-1{border-color:', $css);
			}

			if(isset($options['siteinfo-line-separator-layout']) && $options['siteinfo-line-separator-layout'] == 1) {
				$css = str_replace('#site-info{border-color:', '#site-info>.container{border-color:', $css);
			} else {
				$css = str_replace('#site-info>.container{border-color:', '#site-info{border-color:', $css);
			}

			if(isset($options['titlebar-padding'])) {
				$css .= '#page-title-bar { padding-top: '.$options['titlebar-padding'].'px;}';
			}
			if(isset($options['titlebar-padding-bottom'])) {
				$css .= '#page-title-bar { padding-bottom: '.$options['titlebar-padding-bottom'].'px;}';
			}

			//Footer
			if(isset($options['footer-margin-top']) && is_numeric($options['footer-margin-top'])) {
				$css .= '#colophon { margin-top: '.$options['footer-margin-top'].'px;}';
			}

			//Featured font additional delcaration
			if(isset($options['typography-primary-text']) && isset($options['typography-primary-text']['font-family'])) {
				$css .= '#popup-box-sxzw-1 .popupally-inner-sxzw-1 .desc-sxzw,
					#popup-embedded-box-sxzw-1 .popupally-inner-sxzw-1 .desc-sxzw,
					#popup-box-sxzw-1 .popupally-inner-sxzw-1 .popupally-center-sxzw .content-sxzw input[type="submit"].submit-sxzw,
					#popup-embedded-box-sxzw-1 .popupally-inner-sxzw-1 .popupally-center-sxzw .content-sxzw input[type="submit"].submit-sxz,
					#lang_sel_list { font-family: '.$options['typography-primary-text']['font-family'].' !important}';
			}

			//Menu additional declaration
			if(isset($options['typography-main-menu']) && isset($options['typography-main-menu']['font-size'])) {
				$css .= '.primary-menu-icons > div,
					#primary-menu > li > a,
					#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a {
                        font-size: '.$options['typography-main-menu']['font-size'].';
					}';
			}

			//Header additional styles
			if(isset($options['header-background-image']) && !empty($options['header-background-image']['background-image'])) {
				$css .= '#header-wrap, #header-wrap:not(.is-sticky) #primary-navigation, #actionbar { background-color: transparent; }';
//				$css .= '/*#masthead:not(.overlapping-header),*/
//				        #masthead.overlapping-header #header-wrap:not(.is-sticky)
//				         { background-color: '.$options['header-background'].'}';
//				$css .= '#masthead.overlapping-header #actionbar
//				         { background-color: '.$options['actionbar-background'].'}';
			}

			//Primary font declaration
			if(isset($options['typography-primary-text'])) {
				$backup_font = (isset($options['typography-primary-text']['font-backup']) && !empty($options['typography-primary-text']['font-backup'])) ? ','.$options['typography-primary-text']['font-backup'] : '';
				$css .= '
				h1,
				.h1,
				h2,
				.h2,
				h3,
				.h3,
				h4,
				.h4,
				h5,
				.h5,
				h6,
				.h6,
				.schon-featured-info-title,
				.woocommerce span.onsale,
				#page ul.cart_list li .product-title,
				#page ul.product_list_widget li .product-title,
				#page .widget_recent_reviews a,
				#page ul.cart_list li a,
				#page ul.product_list_widget li a,
				.single-product-wishlist-container a,
				.woocommerce div.product .woocommerce-tabs ul.tabs li a,
				.schon-featured-banner .schon-featured-banner-badge,
				.vc_tta .vc_tta-tab>a,
				.btn,
				.button,
				ul.products.list li.product .gridlist-buttonwrap a.link
				{ font-family: '.$options['typography-primary-text']['font-family'].$backup_font.';}';
			}

			//General font declaration
			if(isset($options['typography-text'])) {
				$css .= 'body { font-size: '.$options['typography-text']['font-size'].'}';

				$css .= 'body,
				#primary-menu ul li a,
				#actionbar .staticblocks-section .staticblock-field,
				#page ul.products li.product h3,
				#page ul.products li.product .add-to-cart-buttons-container a.add-to-cart-button,
				#popup-box-sxzw-1 .popupally-inner-sxzw-1 .logo-text-sxzw,
				#popup-embedded-box-sxzw-1 .popupally-inner-sxzw-1 .logo-text-sxzw
				 { font-family: '.$options['typography-text']['font-family'].'}';

				$css .= '
				#popup-box-sxzw-1 .popupally-inner-sxzw-1 .logo-text-sxzw,
				#popup-embedded-box-sxzw-1 .popupally-inner-sxzw-1 .logo-text-sxzw,
				#popup-box-sxzw-1 .popupally-inner-sxzw-1 .privacy-sxzw,
				#popup-embedded-box-sxzw-1 .popupally-inner-sxzw-1 .privacy-sxzw
				{font-family: '.$options['typography-text']['font-family'].' !important}';
			}

			/* Colors declarations */
			if(isset($options['primary-color'])) {
				$css .= '
				.btn-primary:hover,
				.btn-primary:focus,
				.btn-primary:active,
				.btn-primary:active:hover,
				.btn-primary:active:focus {
				background-color: '.schon_adjust_color($options['primary-color'], -51).'}';
			}

			if(isset($options['secondary-color'])) {
				$css .= '
				.vc_tta .vc_tta-tab.vc_active>a,
				.vc_tta .vc_tta-tab>a:hover {
				color: '.$options['secondary-color'].' !important}';

				$css .= '
				.woocommerce div.product .woocommerce-tabs ul.tabs li:hover,
				.woocommerce div.product .woocommerce-tabs ul.tabs li.active,
				#schon-shop-steps-wrapper .step-number,
				.vc_tta .vc_tta-tab.vc_active>a,
				.vc_tta .vc_tta-tab>a:hover,
				.schon-owl-theme .owl-theme .owl-buttons div:hover  {
				border-color: '.$options['secondary-color'].' !important}';

				$css .= '
				.btn-primary-2:hover,
				.btn-primary-2:focus,
				.btn-primary-2:active,
				.btn-primary-2:active:hover,
				.btn-primary-2:active:focus {
				background-color: '.schon_adjust_color($options['secondary-color'], 210).'}';
			}
			
			if(isset($options['actionbar-text-color'])) {
				$css .= '#actionbar .staticblocks-section .staticblock-field,
					#actionbar .staticblocks-section .staticblock-field input,
					#actionbar .search-form .form-group .woocommerce-product-search-categories-filter,
					#actionbar .submit-field,
					#actionbar a,
					#actionbar-menu a,
					.staticblocks-section #lang_sel_list.lang_sel_list_horizontal li a,
					#lang_sel a.lang_sel_sel{ color: '.$options['actionbar-text-color']['regular'].'}';
				$css .= '#actionbar .staticblocks-section .staticblock-field input,
				    #actionbar .search-form .form-group .woocommerce-product-search-categories-filter { border-color: '.$options['actionbar-text-color']['regular'].'}';
				$css .= '#actionbar-menu li:before { background-color: '.$options['actionbar-text-color']['regular'].'}';
				$css .= '#actionbar a:hover, #actionbar-menu a:hover, .staticblocks-section #lang_sel_list.lang_sel_list_horizontal li a:hover { color: '.$options['actionbar-text-color']['hover'].'}';
			}

			// Primary menu
			if(isset($options['main-menu-link-color'])) {
				$css .= '#primary-menu a,
					#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a,
					.mini-cart-header .mini-cart-icon a,
					.primary-menu-icon > a
					{ color: '.$options['main-menu-link-color']['regular'].';}';
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:after,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:after
					{ background-color: '.$options['main-menu-link-color']['regular'].';}';
				$css .= '#primary-menu a:hover,
					#primary-menu a:focus,
					#primary-menu a:active,
					#primary-menu ul li > a:hover,
					#primary-menu ul li > a:active,
					#primary-menu ul li > a:focus,
					.mini-cart-header:hover .mini-cart-icon a,
					.mini-cart-header.active .mini-cart-icon a,
					.mini-cart-header:focus .mini-cart-icon a,
					.mini-cart-header:active .mini-cart-icon a,
					.primary-menu-icon > a:hover,
					.primary-menu-icon > a:focus,
					.primary-menu-icon > a:active,
					#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:hover,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:hover
					{ color: '.$options['main-menu-link-color']['hover'].';}';
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:hover:after,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:hover:after
					{ background-color: '.$options['main-menu-link-color']['hover'].';}';
			}

			if(isset($options['headings-megamenu-link-color'])) {
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a {color: '.$options['headings-megamenu-link-color']['regular'].';}';
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:after,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:after
					{ background-color: '.$options['headings-megamenu-link-color']['regular'].';}';
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:hover,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:hover,
					 #primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:focus,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:focus{color: '.$options['headings-megamenu-link-color']['hover'].';}';
				$css .= '#primary-menu > li.megamenu > div.sub-menu-wrap > ul > li > a:hover:after,
					#primary-menu  li.megamenu li.megamenu-column > div.sub-menu-wrap > ul > li > a:hover:after
					{ background-color: '.$options['headings-megamenu-link-color']['hover'].';}';
			}


			if(isset($options['siteinfo-text-color'])) {
				$css .= '#site-info, #site-info a { color: '.$options['siteinfo-text-color']['regular'].'}';
				$css .= '#site-info a:hover { color: '.$options['siteinfo-text-color']['hover'].'}';
			}

			/* Colors declarations */

			//$css = preg_replace('/.wf-loading (.*) }/','',$css);
			//$css = str_replace('.wf-loading }', '', $css);

			$css = str_replace(',{', '{', $css);

			$wp_filesystem->put_contents(
				$filename,
				$css,
				FS_CHMOD_FILE // predefined mode settings for WP files
			);
		}

	}
}

function shon_remove_redux_notices() { // Be sure to rename this function to something more unique
	if ( class_exists('ReduxFrameworkPlugin') ) {
		remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
	}
	if ( class_exists('ReduxFrameworkPlugin') ) {
		remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
	}
}
add_action('init', 'shon_remove_redux_notices');
