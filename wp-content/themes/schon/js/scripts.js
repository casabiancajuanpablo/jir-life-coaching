'use strict';
(function($){

    var header_position_top = '61px';
    var header_height = 0;

    var header_placeholder = $('#header-placeholder');
    var header = $('#header-wrap');


    function schon_sticky(){
        if( $('body').hasClass('sticky-header') ){

            var header_height = header.height();

            var start_scroll = header_height;
            var window_scroll = $(window).scrollTop();

            if( window_scroll > start_scroll ){
                if( ! ($('#header-wrap').hasClass('is-sticky'))){

                    header_placeholder.css('height', $('#header-wrap').innerHeight());

                    var top = (window.matchMedia("(max-width: 600px)").matches) ? 0 : $('#wpadminbar').innerHeight();

                    $('#header-wrap')
                        .addClass('is-sticky')
                        .css('top',-60)
                        .animate({
                            'top': top
                        },300);
                }
            }
            else {
                if($('#header-wrap').hasClass('is-sticky')) {
                    header_placeholder.css('height',0);
                    $('#header-wrap')
                        .removeClass('is-sticky')
                        .css('top', header_position_top);
                }
            }
        }
    }

    function align_banners() {
        var $banners = $('.schon-featured-banner');
        $banners.each(function(){

            var $badge = $(this).find('.schon-featured-banner-badge');
            if($badge.length > 0) {
                if(parseInt($badge.outerWidth()) > parseInt($badge.outerHeight() + 1)) {
                    var new_padding = parseInt(($badge.width() - $badge.height()) / 2 + 10) + 'px';
                    $badge.css('padding-top', new_padding).css('padding-bottom', new_padding);
                }
            }


            var $inner = $(this).find('.schon-featured-banner-inner');
            if($inner.length > 0) {
                var inner_height = $(this).height();
                var inner_width = $(this).width();
                $inner.each(function(){
                    if($(this).hasClass('v-center')) {
                        $(this).css('top', parseInt(( inner_height - $(this).outerHeight()) / 2) + 'px');
                    }
                    if($(this).hasClass('h-center')) {
                        $(this).css('left', parseInt((inner_width - $(this).outerWidth()) / 2) + 'px');
                    }
                });
            }

            var $image = $(this).find('.attached_image');

            if($image.height() < $image.find('img').height())
                $image.find('img').css('max-height', $image.height());

            if($image.hasClass('v-center')) {
                $image.css('margin-top', parseInt(($(this).outerHeight() - $image.outerHeight()) / 2) + 'px');
            }
            if($image.hasClass('h-center')) {
                $image.css('margin-left', parseInt(($(this).width() - $image.outerWidth()) / 2) + 'px');
            }



        });
    }

    function align_single_product_summary() {
        if(window.matchMedia("(min-width: 769px)").matches) {
            $('.woocommerce div.product .summary .summary-content').css('min-height', $('.woocommerce div.product div.images').height())
        } else {
            $('.woocommerce div.product .summary .summary-content').css('min-height', '0');
        }
    }

    function align_footer_widgets() {
        if(window.matchMedia("(min-width: 601px)").matches) {
            $("#footer-widgets-row-1 .footer-widget-column").height(Math.max.apply(null, $("#footer-widgets-row-1 .footer-widget-column").map(function () {
                return $(this).height();
            }).get()));

            $("#footer-widgets-row-2 .footer-widget-column").height(Math.max.apply(null, $("#footer-widgets-row-2 .footer-widget-column").map(function () {
                return $(this).height();
            }).get()));
        } else {
            $("#footer-widgets-row-1 .footer-widget-column").height('auto');
            $("#footer-widgets-row-2 .footer-widget-column").height('auto');
        }
    }

    function handle_footer_accordion() {

        var $widgets_area = $('#colophon.schon-accordion-enabled .footer-widget-column');
        if($widgets_area.length == 0)
            return;

        var instance = $widgets_area.accordion('instance');

        if( window.matchMedia("(max-width: 600px)").matches) {

            if( !instance)
                $('.footer-widget-column').accordion({
                    header: ".widget-title",
                    collapsible: true,
                    active: false
                });
        } else {
           if(instance)
               $('.footer-widget-column').accordion('destroy');
        }

    }

    function single_product_layout_background() {
        // if($('body').hasClass('single-product-background-layout_3')) {
        //     var $single_product_header = $('.woocommerce.single-product-background-layout_3 div.product .single-product-header')
        //     $single_product_header.append('<div class="single-product-header-background"></div>');
        //     var background_offset = $single_product_header.offset();
        //     $single_product_header.find('.single-product-header-background').width($('#page').width()-2).css('left', '-' + parseInt(background_offset.left - 1) + 'px');
        // }
    }
    
    $(window).on("load",function(){
        if($('.mini-cart-header').length > 0) {
            $(".mini-cart-header .inner").mCustomScrollbar({
                'theme': 'dark',
            });

            $('.mini-cart-header').on('mouseenter', function () {
                $(".mini-cart-header .inner").mCustomScrollbar('update');
            });
        }

        if( $("#secondary").length > 0) {
            $("#secondary").mCustomScrollbar({
                'theme': 'dark',
            });
        }


        var $product_thumbnails = $('#page .product .images .flex-control-nav');

        if($product_thumbnails.length > 0) {
            $($product_thumbnails).owlCarousel({
                'itemsMobile' : 3,
                'pagination' : false,
                'navigation' : true,
                'afterAction' : function(){
                    var $next = $product_thumbnails.find('.owl-next'),
                        $prev = $product_thumbnails.find('.owl-prev');
                    if ( this.itemsAmount > this.visibleItems.length ) {
                        $next.show();
                        $prev.show();

                        $next.removeClass('hidden');
                        $prev.removeClass('hidden');
                        if (this.currentItem == 0) {
                            $prev.addClass('hidden');
                        }
                        if (this.currentItem == this.maximumItem) {
                            $next.addClass('hidden');
                        }
                    }
                }
            });

            /* Removed after WooCommerce 3.0.0 */
            // var $thumbs = $product_thumbnails.find('.owl-item');
            // $thumbs.on('mouseover', function() {
            //     $thumbs.addClass('not-hover');
            //     $(this).removeClass('not-hover');
            // }).on('mouseleave', function(){
            //     $thumbs.removeClass('not-hover');
            // });

        }

        var $products = $('ul.products li.product .woocommerce-product-image-container');

        /*
        $products.on('mouseover', function(){
            $('ul.products li.product').addClass('not-hover');
            $(this).closest('.product').removeClass('not-hover');
        }).on('mouseleave', function(){
            $('ul.products li.product').removeClass('not-hover');
        });*/

        
        var $masonry_blog = $('#main.layout-3 .blog-posts-container');
        if($masonry_blog.length > 0) {
            var masonry_blog_iso = new Isotope( $masonry_blog, {
                // options
                itemSelector: 'article',
                masonry: {
                    columnWidth: '.grid-sizer',
                    gutter: '.gutter-sizer'
                }
            });
        }

        align_single_product_summary();

        align_banners();

        align_footer_widgets();

        single_product_layout_background();
    });
    //onLoad

    $(window).on('resize', function(){
        $('#yith-quick-view-modal .summary').css('min-height', $('.yith-wcqv-main').outerHeight());

        align_single_product_summary();

        align_banners();

        handle_footer_accordion();

        align_footer_widgets();

        single_product_layout_background();
    });
    //onResize

    $(window).scroll(function(){
        schon_sticky();
    });
    //onScroll

    handle_footer_accordion();

    $('body').on( 'added_to_cart', function(){
       if($('body').hasClass('minicart_layout_1')){
           $('.mini-cart-header').addClass('active');
           $(".mini-cart-header .inner").mCustomScrollbar('update');
           setTimeout(function(){
               $('.mini-cart-header').removeClass('active');
           }, 1500);
       } else {
           $('.mini-cart-header').toggleClass('fixed-opened');
       }
    });

    $("#primary-menu li").on('mouseenter', function (e) {
        var elm = $(this).find('.sub-menu-wrap').first();
        if(elm.length) {
            var off = elm .offset();
            var l = off.left;
            var w = elm.outerWidth();
            var docW = $('#page').innerWidth();

            var isEntirelyVisible = (l+ w+20 <= docW);
            var margin_left = $(elm).css('margin-left');
            if(!isEntirelyVisible) {
                var difference = parseInt(docW - (l+ w+20)) + 'px';
                $(elm).css({'margin-left': difference});

                var arrow = $(elm).find('.sub-menu-wrap-arrow').first();
                var arrow_diff = parseInt($(arrow).css('left')) + parseInt(difference) * -1;
                $(arrow).css('left', arrow_diff +'px' );
            }

        }
    });

    $("#primary-menu li").on('mouseleave', function (e) {
        var elm = $(this).find('.sub-menu-wrap').first();
        if (elm.length) {
            $(elm).removeAttr('style');

            var arrow = $(elm).find('.sub-menu-wrap-arrow').first();
            $(arrow).removeAttr('style');
        }
    });

    $("#navbar-search-trigger").on('click', function () {
        $("#search-fullpage-wrap").toggleClass('active');
        $("body").toggleClass('search-fullpage-active');
        $("#navbar-search-trigger").blur();
        $(".search-fullpage-form-wrapper .search-field").focus();
    });
    $(".close-search-fullpage-trigger").on('click', function () {
        $("#search-fullpage-wrap").removeClass('active');
        $("body").removeClass('search-fullpage-active');
        $(".search-fullpage-form-wrapper .search-field").val('');
    });

    $(".navbar-toggle").on('click', function () {
        $("body").toggleClass('overflow-hidden');
        $(".navbar-collapse").toggleClass('opened');
    });
    $(".primary-menu-resposive-overlay").on('click', function(){
        $(".navbar-toggle").click();
    });

    $("#primary-menu .submenu-arrow").on('click', function(){
        var item = $(this).closest('li');
        item.toggleClass('opened');
    });

    $("#sidebar-trigger,#sidebar-trigger-responsive").on('click', function(){
        $('body').toggleClass('schon-sidebar-opened');
        $("#secondary").mCustomScrollbar('update');
    });
    $(".sidebar-overlay").on('click', function(){
        $('body').removeClass('schon-sidebar-opened');
    });
    $("#close-sidebar-trigger").on('click', function(){
        $('body').removeClass('schon-sidebar-opened');
    });

    if($('body').hasClass('minicart_layout_2') && window.matchMedia("(min-width: 769px)").matches) {
        $('#primary-navigation').on('click', '.cart-contents', function(e){
            $('.mini-cart-header').toggleClass('fixed-opened');

            e.preventDefault();
        });

        $('#primary-navigation').on('click', '.mini-cart-header .mini-cart-overlay, #close-minicart-trigger', function(e){
            $('.mini-cart-header .cart-contents').click();
        });
    }

})(jQuery);

/************************ WOOCOMMERCE *********************************/
// var createCookie = function(name, value, days) {
//     var expires;
//     if (days) {
//         var date = new Date();
//         date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//         expires = "; expires=" + date.toGMTString();
//     }
//     else {
//         expires = "";
//     }
//     document.cookie = name + "=" + value + expires + "; path=/";
// }
//
// function getCookie(c_name) {
//     if (document.cookie.length > 0) {
//         c_start = document.cookie.indexOf(c_name + "=");
//         if (c_start != -1) {
//             c_start = c_start + c_name.length + 1;
//             c_end = document.cookie.indexOf(";", c_start);
//             if (c_end == -1) {
//                 c_end = document.cookie.length;
//             }
//             return unescape(document.cookie.substring(c_start, c_end));
//         }
//     }
//     return "";
// }