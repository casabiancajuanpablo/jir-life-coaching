<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package schon
 */

$secondaryClass = schon_get_secondary_class();

if ( ! is_active_sidebar( 'sidebar-page' ) || is_null($secondaryClass)) {
	return;
}
?>


<aside id="secondary" class="widget-area <?php echo $secondaryClass ?>" role="complementary">
	<?php dynamic_sidebar( 'sidebar-page' ); ?>
</aside><!-- #secondary -->
