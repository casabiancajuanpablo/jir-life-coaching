<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package schon
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>



</head>


<body <?php body_class(); ?>>

<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'schon' ); ?></a> -->
	

	<header id="masthead" class="<?php echo apply_filters('schon_masthead_classes', 'site-header')?>" style="<?php echo apply_filters('schon_masthead_inline_style', '')?>"role="banner">

		<div id="header-placeholder"></div>

		<div id="header-wrap">
			<div id="search-fullpage-wrap" >
				<div class="search-fullpage-form-wrapper">
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
						<label>
							<span class="screen-reader-text"><?php esc_html_e('Search', 'schon')?></span>
							<input type="search" class="search-field" autocomplete="off" placeholder="<?php esc_html_e('Search', 'schon'); ?>" value="" name="s" title="<?php esc_html_e('Search', 'schon'); ?>" />
						</label>
						<button type="submit" class="submit-field">
							<i class="icon-magnifier"></i>
						</button>
						<input type="hidden" name="post_type" value="product" />
					</form>
					<div class="close-search-fullpage-trigger-wrap"><a class="close-search-fullpage-trigger" href="javascript:void(0)" ><i class="fa fa-times"></i></a></div>
				</div>
			</div>
			<?php if(schon_get_option('show-action-bar')): ?>
			<div id="actionbar">
				<div class="container">
					<?php
					schon_static_blocks('staticblock-actionbar-left-fields', 'actionbar-fields-left');
					
					schon_static_blocks('staticblock-actionbar-right-fields', 'actionbar-fields-right');
					?>

				</div>
			</div>
			<?php endif; ?>

			<?php
			//Include the navigation template
			$header_layout = schon_get_option('header_layout', 'layout_1');

			switch ( $header_layout ) {
				case 'layout_1':
					get_template_part('template-parts/header/navigation-1');
					break;
				case 'layout_2':
					get_template_part('template-parts/header/navigation-2');
					break;
				case 'layout_3':
					get_template_part('template-parts/header/navigation-3');
					break;
			}

			?>

		</div><!-- #header-wrap -->

	</header><!-- #masthead -->
			
	

	<div id="content" class="site-content">
		<?php
		schon_current_page_title( );
		?>