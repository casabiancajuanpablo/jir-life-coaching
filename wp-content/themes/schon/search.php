<?php
/**
 * The template for displaying search results pages.
 *
 * @package schon
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			
			<div id="primary" class="content-area <?php echo schon_get_primary_class() ?>">
				<main id="main" class="site-main" role="main">

					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php 
								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'content', 'search' ); 
							?>

		
						<?php endwhile; // end of the loop. ?>

						<?php the_posts_navigation(); ?>

					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->
			

			<?php schon_show_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>