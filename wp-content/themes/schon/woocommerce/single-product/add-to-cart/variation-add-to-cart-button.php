<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">

	<?php
	/**
	 * @since 3.0.0.
	 */
	do_action( 'woocommerce_before_add_to_cart_quantity' ); ?>

	<div class="form-group form-group-add-to-cart">
		<label class="qty-label">
			<?php esc_html_e('qty:', 'schon'); ?>
		</label>

		<?php woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) ); ?>

		<button type="submit" class="single_add_to_cart_button btn btn-lg btn-primary"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
	</div>

	<?php
	/**
	 * @since 3.0.0.
	 */
	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->id ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
