<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product );

?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php
	/**
	 * @since 2.1.0.
	 */
	do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<form class="cart form-inline" method="post" enctype='multipart/form-data'>
	 	<?php
	    /**
		 * @since 2.1.0.
		 */
	    do_action( 'woocommerce_before_add_to_cart_button' );

	    /**
		 * @since 3.0.0.
		 */
	    do_action( 'woocommerce_before_add_to_cart_quantity' );
	    ?>

		<div class="form-group form-group-add-to-cart">
			<label class="qty-label">
				<?php esc_html_e('qty:', 'schon'); ?>
			</label>
			<?php
			woocommerce_quantity_input( array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
			) );
			?>

			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" class="single_add_to_cart_button btn btn-lg btn-primary"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
		</div>

		<?php
		/**
		 * @since 2.1.0.
		 */
		do_action( 'woocommerce_after_add_to_cart_button' );
		?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
