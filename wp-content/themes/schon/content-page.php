<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package schon
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php

		if ( has_post_thumbnail() ) {
			$medium_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
			$large_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
			$full_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );

			echo '<picture>';
			echo '<source srcset="'.esc_url($full_src[0]).'" media="(min-width: 641px)" >';
			echo '<source srcset="'.esc_url($large_src[0]).'" media="(min-width: 301px)" >';
			echo '<img src="'.$medium_src[0].'">';
			echo '</picture>';
		}

		?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'schon' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                /* translators: %s: Name of current post */
                    esc_html__( 'Edit %s', 'schon' ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-## -->
