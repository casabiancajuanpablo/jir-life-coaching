<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div>
		<label class="screen-reader-text" for="s">Cerca:</label>
		<div class="input-group">
			<input type="text" value="" placeholder="<?php esc_html_e('Search', 'schon')?>" name="s" id="s" class="search-field form-control"/>
			<div class="input-group-btn">
				<input type="submit" id="searchsubmit" value="<?php esc_html_e('Search', 'schon')?>" class="search-submit btn btn-default"/>
			</div>
		</div>
	</div>
</form>