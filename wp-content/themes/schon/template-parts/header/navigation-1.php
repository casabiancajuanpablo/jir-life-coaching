<nav id="primary-navigation" class="navbar">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<div class="responsive-navigation-triggers">
				<button type="button" class="navbar-toggle collapsed" >
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<?php
				$header_option = schon_get_sidebar_option();
				if( $header_option != 1): ?>
					<a id="sidebar-trigger-responsive" href="javascript:void(0)"><i class="icon icon-options-vertical"></i></a>
				<?php endif; ?>
			</div>

			<div class="navbar-brand site-brand">
				<?php schon_branding(); ?>
			</div><!-- .site-branding -->

			<?php schon_static_blocks('staticblock-right-logo'); ?>
		</div>


		<div class="primary-menu-icons navbar-nav pull-right">

			<?php get_template_part('template-parts/header/icons'); ?>

		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="navbar-collapse" id="primary-navigation-menu-wrap">


			<!--						<div class="navbar-right">-->
			<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav navbar-right', 'container' => '') ); ?>
			<?php
			if(has_nav_menu( 'primary' )) {
//				$navbar_align = (schon_get_option('primary-menu-align') == '2') ? 'navbar-right' : '';
				$navbar_align = 'pull-right';
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => "nav navbar-nav $navbar_align", "container" => "", "walker" => new schon_Submenu_Wrap() ) );
			} else
				echo "<p class=\"navbar-text pull-right\">".esc_html__('Register a primary navigation menu.', 'schon')."</p>";

			?>
			<!--						</div>-->

			<div class="primary-menu-resposive-overlay"></div>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>