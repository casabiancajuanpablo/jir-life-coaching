<?php

if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );
?>

<?php if(schon_get_option('show-search-primary-navigation')): ?>
	<div class="search-icon primary-menu-icon">
		<a id="navbar-search-trigger" href="javascript:void(0)"><i class="icon icon-magnifier"></i></a>
	</div>
<?php endif; ?>

<?php if(schon_is_wishlist_enabled() && schon_get_option('show-wishlist-primary-navigation')): ?>
	<div class="wishlist-icon primary-menu-icon">
		<a href="<?php echo esc_url(YITH_WCWL()->get_wishlist_url()); ?>"><i class="icon icon-heart"></i></a>
	</div>
<?php endif; ?>

<?php
/**
 * Check if WooCommerce is active
 **/
if (
	in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )
	&& schon_get_option('show-cart-primary-navigation')
	&& !schon_is_catalog_mode_enabled()
): ?>
	<?php global $woocommerce;
	$products_number = 	$woocommerce->cart->cart_contents_count;
	?>
	<div class="mini-cart-header primary-menu-icon widget_shopping_cart">
		<div class="mini-cart-icon">
			<a class="cart-contents" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" title="<?php esc_html_e('View your shopping cart', 'woothemes'); ?>">
				<i class="icon icon-handbag"></i>
				<span><?php echo $products_number; ?></span>
			</a>
		</div>

		<div class="inner <?php if($products_number == 0) echo 'cart-empty'?>">
			<?php woocommerce_mini_cart() ?>
		</div>

		<div class="mini-cart-overlay"></div>
	</div>
<?php endif; ?>

<?php
$sidebar_option = schon_get_sidebar_option();
if( $sidebar_option == 4 ): ?>
<div class="sidebar-trigger-icon primary-menu-icon">
	<a id="sidebar-trigger" href="javascript:void(0)"><i class="icon icon-options-vertical"></i></a>
</div>
<?php endif; ?>