<?php
/**
 * @package schon
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
	<header class="entry-header">
		<?php
		if ( !is_single() )
		echo '<a href="'.esc_url( get_permalink() ) .'" rel="bookmark" class="featured-image-link">';

		if ( has_post_thumbnail() ) {
			$medium_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
			$large_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
			$full_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );

			echo '<picture>';
			echo '<source srcset="'.esc_url($full_src[0]).'" media="(min-width: 641px)" >';
			echo '<source srcset="'.esc_url($large_src[0]).'" media="(min-width: 301px)" >';
			echo '<img src="'.$medium_src[0].'">';
			echo '</picture>';
		}

		if ( !is_single() )
		echo '</a>';
		?>

		<div class="entry-title-container">
			<?php
			if ( is_single()) {
				if(!schon_get_option('show-titlebar-single-post'))
					the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
			?>
			<?php if ( 'post' == get_post_type() ) : ?>
				<div class="entry-meta">
					<?php schon_entry_meta(); ?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</div>
		
        
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
                wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'schon' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'schon' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php schon_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->