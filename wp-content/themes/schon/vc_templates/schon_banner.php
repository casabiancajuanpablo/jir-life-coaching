<?php
// Attributes
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_banner', $atts );
}
else {
	$atts = shortcode_atts(
		array(
			'height' => 'height_layout_1-1',
			'background_color' => '#f0f0f0',
			'background_image' => '',
			'background_image_style' => '',

			'content',
			'content_alignment' => '',
			'content_width' => '',
			'content_force_color' => '',

			'attached_image' => '',
			'image_alignment' => '',
			'image_width' => '',
			//	'image_vertical_offset' => 0,
			//	'image_horizontal_offset' => 0,

			'link_to' => '',
			'create_button' => '',
			'hover_style' => 'darker',
			'button_anchor' => '',
			'button_icon' => '',
			'button_style' => '',
			'button_size' => '',
			'button_alignment' => '',
			'link_force_color' => '',

			'badge_text' => '',
			'badge_background' => '',
			'badge_alignment' => '',
			'badge_size' => '',

			'el_class' => '',
			//'css' => '',
		),
		$atts,
		'schon_banner'
	);
}


$banner_style = '';
//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );

$banner_style .= (!empty($atts['background_color'])) ? 'background-color: ' . $atts['background_color'] .';'  : '';

if(!empty($atts['background_image'])) {
	$background_image_url =  wp_get_attachment_image_src( $atts['background_image'], 'large' );
	$banner_style .= 'background-image: url(' . $background_image_url[0] . ');';
}

if(!empty($atts['background_image_layout'])) {
	$banner_style .= ($atts['background_image_layout'] == 'cover' || $atts['background_image_layout'] == 'contain') ? 'background-size: ' . $atts['background_image_layout'].';'  : 'background-repeat: ' . $atts['background_image_layout'].';';
}

$outer_classes =  $atts['height'];
$outer_classes .= ' ' . $atts['el_class'];
//$outer_classes .= ' ' . $css_class;

$content_classes = $atts['content_width'];
$content_classes .= ' ' . $atts['content_alignment'];
$content_classes .= ' ' . $atts['content_force_color'];

$image_classes = $atts['image_alignment'];
$image_classes .= ' ' . $atts['image_width'];

$image_url = wp_get_attachment_image_src( $atts['attached_image'], 'large' );


//$image_padding = 'padding: ' .intval($atts['image_vertical_offset']).'px ' . intval($atts['image_horizontal_offset']).'px;';

$link_to = ($atts['link_to'] != '#' && substr($atts['link_to'], 0, 3) == 'www') ? 'http://'.$atts['link_to'] : $atts['link_to'];

$button_html = '';
$button_icon = '';
if(!empty($atts['button_icon'])) {
	$button_icon = ' <i class="'.$atts['button_icon'].'"></i>';
}


if($atts['create_button'] == true && !empty($link_to)) {
	$link_force_color = (!empty($atts['link_force_color'])) ? 'style="color: ' . $atts['link_force_color'] .';"' : '';
	$button_html = '<div class="schon-featured-banner-button schon-featured-banner-inner '.$atts['button_alignment'].'"><a class="banner-btn btn ' . $atts['button_style'].' ' .  $atts['button_size'] .'" href="'.esc_url($link_to).'" '.$link_force_color.'>'.esc_html($atts['button_anchor']).$button_icon.'</a></div>';
}

$badge_html = '';
if(!empty($atts['badge_text'])) {
	$badge_html .= '<div class="schon-featured-banner-inner '.$atts['badge_alignment'].'">';
	$badge_html .= '<div class="schon-featured-banner-badge '.$atts['badge_size'].'" style="background-color:'.$atts['badge_background'].'">';
	$badge_html .= esc_html($atts['badge_text']);
	$badge_html .= '</div></div>';

}
?>



<div class="schon-featured-banner <?php echo esc_attr($outer_classes) ?>" style="<?php echo $banner_style; ?>">
	<?php
	if($atts['create_button'] != true && !empty($link_to)) {
		echo '<div class="schon-featured-banner-link-wrap"><a class="schon-featured-banner-link" href="' . esc_url( $link_to ) . '">';
		$link_force_color = (!empty($atts['link_force_color'])) ? 'style="background-color: ' . $atts['link_force_color'] .';"' : '';
		echo '<div class="overlay-link '.esc_attr($atts['hover_style']).'" '.$link_force_color.'></div>';
	}
	?>
	<div class="attached_image <?php echo esc_attr($image_classes); ?>"><img src="<?php echo esc_url($image_url[0])?>"></div>

	<div class="schon-featured-banner-content schon-featured-banner-inner <?php echo esc_attr($content_classes); ?>">
		<?php echo do_shortcode($content); ?>
	</div>

	<?php echo $button_html ?>

	<?php echo $badge_html; ?>

	<?php
	if($atts['create_button'] != true && !empty($link_to)) {
		echo '</a></div><!--schon-featured-banner-link-wrap-->';
	}
	?>
</div>