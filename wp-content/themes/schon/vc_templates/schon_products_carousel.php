<?php
// Attributes
$css_class = '';
if(function_exists('vc_map_get_attributes')) {
	$atts = vc_map_get_attributes( 'schon_products_carousel', $atts );
	$css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), $this->settings['base'], $atts );
} else {
	$atts = shortcode_atts(
		array(
			'number' => 10,
			'show' => '',
			'orderby' => 'date',
			'order' => 'DESC',
			'hide_free' => false,
			'show_hidden' => false,
			'categories' => '',

			'number_columns' => 4,
			'rows' => 1,
			'arrows_position' => 'center',

			'el_class' => '',
			'css' => '',
		),
		$atts,
		'schon_products_carousel'
	);
}
//
//$number  = !empty( $atts['number'] ) ? absint( $atts['number'] ) : $this->settings['number']['std'];
//$show    = !empty( $atts['show'] ) ? sanitize_title( $atts['show'] ) : $this->settings['show']['std'];
//$orderby = !empty( $atts['orderby'] ) ? sanitize_title( $atts['orderby'] ) : $this->settings['orderby']['std'];
//$order   = !empty( $atts['order'] ) ? sanitize_title( $atts['order'] ) : $this->settings['order']['std'];

$number  = $atts['number'];
$show    = $atts['show'];
$orderby = $atts['orderby'];
$order   = $atts['order'];
$categories = (isset($atts['categories']) && !empty($atts['categories'])) ? $atts['categories'] : '';

$query_args = array(
	'posts_per_page' => $number,
	'post_status'    => 'publish',
	'post_type'      => 'product',
	'no_found_rows'  => 1,
	'order'          => $order,
	'meta_query'     => array(),
	'tax_query'     => array()
);

if ( ! $atts['show_hidden'] ) {
	$query_args['meta_query'][] = WC()->query->visibility_meta_query();
	$query_args['post_parent']  = 0;
}

if ( $atts['hide_free'] ) {
	$query_args['meta_query'][] = array(
		'key'     => '_price',
		'value'   => 0,
		'compare' => '>',
		'type'    => 'DECIMAL',
	);
}

if ( $categories ) {
	$query_args['tax_query'][] = array(
		'taxonomy'     => 'product_cat',
		'field'   => 'term_id',
		'terms' => $categories,
		'operator'    => 'IN',
	);
}

$query_args['meta_query'][] = WC()->query->stock_status_meta_query();
$query_args['meta_query']   = array_filter( $query_args['meta_query'] );
$query_args['tax_query']   = array_filter( $query_args['tax_query'] );

switch ( $show ) {
	case 'featured' :
		$query_args['meta_query'][] = array(
			'key'   => '_featured',
			'value' => 'yes'
		);
		break;
	case 'onsale' :
		$product_ids_on_sale    = wc_get_product_ids_on_sale();
		$product_ids_on_sale[]  = 0;
		$query_args['post__in'] = $product_ids_on_sale;
		break;
}

switch ( $orderby ) {
	case 'price' :
		$query_args['meta_key'] = '_price';
		$query_args['orderby']  = 'meta_value_num';
		break;
	case 'rand' :
		$query_args['orderby']  = 'rand';
		break;
	case 'sales' :
		$query_args['meta_key'] = 'total_sales';
		$query_args['orderby']  = 'meta_value_num';
		break;
	default :
		$query_args['orderby']  = 'date';
}

$navigation_class = 'navigation-'.$atts['arrows_position'];

$query = new WP_Query($query_args);
$_pf = new WC_Product_Factory();

$item_id = 'owl-products-';
$item_id .= schon_get_rand();

$c = 0;

$css_class = ' ' . $atts['el_class'];
?>

<div class="woocommerce products-carousel-wrapper <?php echo $item_id . ' ' .$css_class; ?>">
	<div class="products-carousel schon-owl-theme">
		<ul class="products owl-products <?php echo $navigation_class; ?>">
			<?php while ($query->have_posts()) : $query->the_post(); ?>
	
				<?php $product = $_pf->get_product($query->get_ID()); ?>
	
				<?php
	
//				if($c++ == 0) {
//					echo '<div class="item">';
//				}
	
				wc_get_template_part( 'content', 'product' );
	
//				if($c == $atts['rows']) {
//					echo '</div>';
//					$c = 0;
//				}
	
				?>
	
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</ul>
		</div>
</div>

<script>
	(function($){
		'use strict';
		
		$(window).load(function() {
			var $owl_products = $('.<?php echo $item_id; ?> .owl-products'),
				rows = <?php echo $atts['rows'] ?>;

			function fix_multirows_items() {
				var $columns = $owl_products.find('.item'),
					rows = $columns.first().find('.product').length;

				$columns.find('.product').height('auto');

				for(var i = 0; i < rows; i++) {
					var $products = $columns.find(' li.product:nth-child('+parseInt((i+1))+')');
					//console.log(':nth-child('+parseInt((i+1))+')');
					var maxHeight = Math.max.apply(null, $products.map(function ()
					{
						return $(this).height();
					}).get());

					$products.height(maxHeight);
				}

			}


			if ($owl_products.length > 0) {
				var $products = $owl_products.find('li');

				for(var i=0; i < $products.length; i+=rows){
					$products.slice(i, i+rows).wrapAll('<div class="item" />');
				}


				$('.vc_tta-tab').click(function(){
					fix_multirows_items;

//					$('.vc_tta-panel .vc_tta-panel-body').css('overflow', 'hidden !important');
//					setTimeout(function(){
//						$('.vc_tta-panel .vc_tta-panel-body').css('overflow', 'visible !important');
//					}, 500)

				});

				$($owl_products).owlCarousel({
					'items': <?php echo $atts['number_columns']; ?>,
					'pagination': false,
					'navigation': true,

					'afterUpdate' : fix_multirows_items,
					'afterInit' : fix_multirows_items
				});
			}
		});
	})(jQuery);

</script>