<?php
/**
 *
 * @package schon
 *
 * Template Name: Full Width
 *
 */

get_header(); ?>
	
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					
					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">

							<?php if ( have_posts() ) : ?>
								
								<?php while ( have_posts() ) : the_post(); ?>

									<?php 
										/* Include the Post-Format-specific template for the content.
										 * If you want to override this in a child theme, then include a file
										 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
										 */
										get_template_part( 'content', get_post_format() ); 
									?>

				
								<?php endwhile; // end of the loop. ?>

								<?php the_posts_navigation(); ?>

							<?php else : ?>

								<?php get_template_part( 'content', 'none' ); ?>

							<?php endif; ?>

						</main><!-- #main -->
					</div><!-- #primary -->
			
				</div>
			</div>
		</div>

<?php get_footer(); ?>