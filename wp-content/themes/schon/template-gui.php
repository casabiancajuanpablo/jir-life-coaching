<?php
/**
 * Twmplate name: GUI
 */

get_header(); ?>
	
	<div class="container">
		<div class="row">
			
			<div class="col-xs-12 image-header-container">
				<?php 
					if ( has_post_thumbnail() ) { // controlla se il post ha un'immagine in evidenza assegnata.
					  //the_post_thumbnail();
					} 
				?>
			</div>

			<?php 
					if ( has_post_thumbnail() ) { 
						$large_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
						$full_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
						?>
			<picture>
                <source media="(max-width: 640px)" src="<?php echo esc_url($large_src[0]); ?>">
                <source media="(min-width: 641px)" src="<?php echo esc_url($full_src[0]); ?>">
			   <!-- <source src="<?php echo $large_src[0]; ?>"> -->
			
			</picture>
			<?php
			} 
			?>			 

			<div id="primary" class="content-area <?php echo schon_get_primary_class() ?>">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page' ); ?>

						<div>
						<a href="#" class="btn btn-default btn-lg">Button</a>
						<a href="#" class="btn btn-default btn-lg btn-ghost">Button</a>
						<a href="#" class="btn btn-default btn-lg btn-small-radius">Button</a>
						<a href="#" class="btn btn-default btn-lg btn-small-radius btn-ghost">Button</a>
						<a href="#" class="btn btn-default btn-lg btn-no-radius">Button</a>
						<a href="#" class="btn btn-default btn-lg btn-no-radius btn-ghost">Button</a>
						</div>
						<div>
							<a href="#" class="btn btn-default" >Button</a>
							<a href="#" class="btn btn-default btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-small-radius">Button</a>
							<a href="#" class="btn btn-default btn-small-radius btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-no-radius">Button</a>
							<a href="#" class="btn btn-default btn-no-radius btn-ghost">Button</a>
						</div>
						<div>
							<a href="#" class="btn btn-default btn-sm">Button</a>
							<a href="#" class="btn btn-default btn-sm btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-sm btn-small-radius">Button</a>
							<a href="#" class="btn btn-default btn-sm btn-small-radius btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-sm btn-no-radius">Button</a>
							<a href="#" class="btn btn-default btn-sm btn-no-radius btn-ghost">Button</a>
						</div>
						<div>
							<a href="#" class="btn btn-default btn-xs">Button</a>
							<a href="#" class="btn btn-default btn-xs btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-xs btn-small-radius">Button</a>
							<a href="#" class="btn btn-default btn-xs btn-small-radius btn-ghost">Button</a>
							<a href="#" class="btn btn-default btn-xs btn-no-radius">Button</a>
							<a href="#" class="btn btn-default btn-xs btn-no-radius btn-ghost">Button</a>
						</div>

						<?php
							if(schon_get_option('show-comments-in-pages')) {
								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
							}
							
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
			

			<?php schon_show_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
