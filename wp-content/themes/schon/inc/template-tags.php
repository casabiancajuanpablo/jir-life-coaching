<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package schon
 */

if ( ! function_exists( 'schon_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function schon_entry_meta() {

	/* Posted on */
	schon_posted_on();
	/* Posted on */

	/* Category */
	/* translators: used between list items, there is a space after the comma */
	$categories_list = get_the_category_list( esc_html__( ', ', 'schon' ) );
	//if ( $categories_list && schon_categorized_blog() ) {
	if ( $categories_list ) {
		echo '<span class="cat-links"><i class="fa fa-list" aria-hidden="true"></i>' . $categories_list . '</span>';
	}
	/* Category */

	/* Author */
	$byline = '<span class="author vcard"><i class="fa fa-user" aria-hidden="true"></i><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>';

	echo '<span class="byline"> ' . $byline . '</span>';
	/* Author */

	/* Comments */
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link"><i class="fa fa-comments-o" aria-hidden="true"></i>';
		/* translators: %s: post title */
		comments_popup_link( esc_html__( 'Leave a Comment', 'schon' ) );
		echo '</span>';
	}
	/* Comments */
}
endif;

if ( ! function_exists( 'schon_entry_footer' ) ) {
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function schon_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' == get_post_type() ) {

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'schon' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">' . esc_html__( 'Tags:  %1$s', 'schon' ) . '</span>', $tags_list );
			}
		}

		edit_post_link(
			sprintf(
			/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'schon' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
}

if(!function_exists('schon_posted_on')) {
	function schon_posted_on() {

		$time_string = '<time class="entry-date published updated" datetime="%1$s"><span class="full">%2$s</span><span class="day h3">%3$s</span><span class="month">%4$s</span></time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s"><span class="full">%2$s</span><span class="day h3">%3$s</span><span class="month">%4$s</span></time><time class="updated" datetime="%5$s">%6$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_html( get_the_date('d') ),
			esc_html( get_the_date('F') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';

		echo '<span class="posted-on"><i class="fa fa-clock-o" aria-hidden="true"></i>' . $posted_on . '</span>';
	}
}

if(!function_exists('schon_modify_read_more_link')) {
	function schon_modify_read_more_link() {
		return '<span class="more-link-wrap"><a class="more-link btn btn-default btn-sm" href="' . get_permalink() . '">'.esc_html__('Read More', 'schon').'</a></span>';
	}
}
add_filter( 'the_content_more_link', 'schon_modify_read_more_link' );

if(!function_exists('schon_paginate_links')) {
	/**
	 * Render the pagination of the current post type
	 */
	function schon_paginate_links() {
		global $wp_query;

		$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
		$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

		echo '<nav class="schon-pagination">';
		echo paginate_links(  array(
			'add_args'     => false,
			'current'      => $current,
			'total'        => $total,
			'prev_text'    => '&larr;',
			'next_text'    => '&rarr;',
			'type'         => 'list',
			'end_size'     => 3,
			'mid_size'     => 3
		)  );
		echo '</nav>';
	}
}

if(!function_exists('schon_render_author_box')) {
	/**
	 * Render the author box in the single post
	 */
	function schon_render_author_box() {
		if(!schon_get_option('blog-show-author-box'))
			return;



		$id = get_the_author_meta('ID');
		$name = get_the_author_meta('nicename');
		$description = get_the_author_meta('description');
		$url = get_the_author_meta('url');


		echo '<aside class="authorbox clearfix">
			'.get_avatar( $id , 146 ).'
			<h4 class="author-name"><a href="'.$url.'">'. $name.'</a></h4>
			<p>' . $description . '</p>
		</aside>';
	      

	}
}

if(!function_exists('schon_move_comment_textarea_to_bottom')) {
	function schon_move_comment_textarea_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;

		return $fields;
	}
}
add_filter( 'comment_form_fields', 'schon_move_comment_textarea_to_bottom' );

//if(!function_exists('schon_wp_list_comments_args')) {
//	function schon_wp_list_comments_args( $args ) {
//
//		$args['style']       = 'div';
//		$args['short_ping']  = true;
//		$args['avatar_size'] = '80';
//		$args['callback']    = 'schon_display_comments_list';
//
//	}
//}
//add_filter( 'wp_list_comments_args', 'schon_wp_list_comments_args' );

if ( ! function_exists( 'schon_display_comments_list' ) ) {
	function schon_display_comments_list($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo esc_html($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? 'media' : 'parent media' ) ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args['style'] ) : ?>
			<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		<?php endif; ?>

		<div class="comment-author vcard media-left">
			<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'], "mystery", "", array('class' => 'media-object') ); ?>

		</div>
		<div class="media-right">
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'schon' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"></a>
			<div class="media-heading">
					<span class="h4">
					<?php
					printf( '<cite class="fn">%s</cite>', get_comment_author_link() );
					?>
					</span>

					<span class="small">
					<?php
					/* translators: 1: date, 2: time */
					printf( '%1$s '. esc_html_x('at', 'Comment published AT 5:00PM','schon'). ' %2$s', get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( __( '(Edit)', 'schon' ), '  ', '' );
					?>
					</span>

			</div>

		</div>

		<?php comment_text(); ?>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
	<?php endif; ?>
		</div>

		<?php
	}
}

if(!function_exists('schon_categorized_blog')) {
	/**
	 * Returns true if a blog has more than 1 category.
	 *
	 * @return bool
	 */
	function schon_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'schon_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories( array(
				'fields'     => 'ids',
				'hide_empty' => 1,

				// We only need to know if there is more than one category.
				'number'     => 2,
			) );

			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );

			set_transient( 'schon_categories', $all_the_cool_cats );
		}

		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so schon_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so schon_categorized_blog should return false.
			return false;
		}
	}
}


if(!function_exists('schon_custom_css')) {
	function schon_custom_css() {
		$customCss = trim( schon_get_option( 'custom-css' ) );

		if ( ! empty( $customCss ) ) {
			echo "<style>";
			echo $customCss;
			echo "</style>";
		}
	}
}
add_action( 'wp_head', 'schon_custom_css', 998 );

if(!function_exists('schon_custom_javascript')) {
	function schon_custom_javascript() {

		$retina_logo = schon_get_option( 'retina-logo-image' );

		if ( ! empty( $retina_logo['url'] )):
			?>
			<script>
				var $j = jQuery.noConflict();
				var $logo = $j('.site-brand .site-title img');

				if (window.devicePixelRatio > 1.3) {

					var width = $logo.width();
					var height = $logo.height();

					$logo.attr("src", "<?php echo $retina_logo['url']?>").width(width).height(height);

				}

			</script>
			<?php
		endif;


		$customJavascript = trim( schon_get_option( 'custom-javascript' ) );

		if ( ! empty( $customJavascript ) ) {
			echo "<script>";
			echo $customJavascript;
			echo "</script>";
		}
	}
}
add_action( 'wp_footer', 'schon_custom_javascript', 998 );

if(!function_exists('schon_branding')) {
	function schon_branding() {

		if ( schon_get_option( 'name-logo' ) ) {
			$brand = get_bloginfo( 'name' );

		} else {

			$logo  = schon_get_option( 'logo-image' );
			$retina_logo = schon_get_option( 'retina-logo-image' );

			$retina_attr = '';
			//todo find a better way to display a retina logo
//			if(!empty($retina_logo['url'])) {
//				$retina_attr = 'srcset="'.$logo['url'].' 1x, '.$retina_logo['url'].' 2x"';
//			}

			$brand = "<img src=\"" . $logo['url'] . "\" ".$retina_attr." alt=\"" . get_bloginfo( 'name' ) . "\">";
		}

		if ( schon_get_option( 'logo-link' ) ) {
			$brand = "<a href=\"" . esc_url( home_url( '/' ) ) . "\" rel=\"home\">" . $brand . "</a>";
		}

		echo "<div class=\"site-title\">$brand</div>";

	}
}

if(!function_exists('schon_show_social_icons')) {
//Todo questa funziione è fatta un po' una chiavica, stampa il contenuto senza container
	function schon_show_social_icons( $echo = true ) {

		$links = array();

		$links['facebook']  = schon_get_option( 'social-facebook' );
		$links['twitter']   = schon_get_option( 'social-twitter' );
		$links['instagram'] = schon_get_option( 'social-instagram' );
		$links['youtube']   = schon_get_option( 'social-youtube' );
		$links['linkedin']  = schon_get_option( 'social-linkedin' );
		$links['dribbble']  = schon_get_option( 'social-dribbble' );
		$links['flickr']    = schon_get_option( 'social-flickr' );
		$links['pinterest'] = schon_get_option( 'social-pinterest' );

		$links  = array_filter( $links );
		$return = '';
		foreach ( $links as $key => $item ) {
			$return .= "<a href=\"$item\" title=\"$key\"><i class=\"fa fa-$key\"></i></a>";
		}

		if ( $echo ) {
			echo $return;
		} else {
			return $return;
		}
	}
}



if(!function_exists('schon_add_custom_favicon')) {
	function schon_add_custom_favicon() {
		$fav = schon_get_option( 'custom-favicon' );

		if ( ! empty( $fav['url'] ) ): ?>
			<link rel="icon" type="image/x-icon" href="<?php echo esc_url($fav['url']); ?>"/>
		<?php endif;
	}
}
add_action( 'wp_head', 'schon_add_custom_favicon' );


if(!function_exists('schon_body_class_sticky_header')) {
	/**
	 * Handle body classes
	 */
	function schon_body_class_sticky_header( $classes ) {

		//Sticky header
		if ( schon_get_option( 'sticky-header' ) ) {
			$classes[] = 'sticky-header';
		}

		//Single product background layout
		$single_product_background_layout = schon_get_option( 'single_product_background_layout' );
		if ( is_product() && ! empty( $single_product_background_layout ) ) {
			$classes[] = 'single-product-background-' . $single_product_background_layout;
		}

		//Header layout
		$header_layout = schon_get_option( 'header_layout' );
		$classes[]     = 'header_' . $header_layout;

		//Minicart version
		$minicart_layout = schon_get_option( 'minicart_layout' );
		$classes[]       = 'minicart_' . $minicart_layout;

		//Blog post date style
//	if(is_home() && schon_get_option('blog-date-as-badge'))
//		$classes[] = 'blog-post-date-badge';

		return $classes;
	}
}
add_filter( 'body_class','schon_body_class_sticky_header' );

if(!function_exists('schon_body_class_boxed')) {
	/**
	 * Add class to body if layout is boxed
	 */
	function schon_body_class_boxed( $classes ) {

		$boxed = schon_get_option( 'boxed-layout' );

		$classes[] = ( empty( $boxed ) ) ? 'wide-layout' : 'boxed-layout';

		return $classes;
	}
}
add_filter( 'body_class','schon_body_class_boxed' );

if(!function_exists('schon_style_boxed_layout')) {
	function schon_style_boxed_layout() {
		$boxed = schon_get_option( 'boxed-layout' );

		//If is boxed
		if ( ! empty( $boxed ) ):


			$boxed_background_type = schon_get_option( 'boxed-background-type' );
			?>
			<style>

				<?php if($boxed_background_type == '1'):
					$color_gradient = schon_get_option('body-background-color');
				?>
				body {
					background: <?php echo $color_gradient['from'] ?>;
					background: -moz-linear-gradient(top, <?php echo $color_gradient['from'] ?> 0%, <?php echo $color_gradient['to'] ?> 100%);
					background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,<?php echo $color_gradient['from'] ?>), color-stop(100%,<?php echo $color_gradient['to'] ?>));
					background: -webkit-linear-gradient(top, <?php echo $color_gradient['from'] ?> 0%,<?php echo $color_gradient['to'] ?> 100%);
					background: -o-linear-gradient(top, <?php echo $color_gradient['from'] ?> 0%,<?php echo $color_gradient['to'] ?> 100%);
					background: -ms-linear-gradient(top, <?php echo $color_gradient['from'] ?> 0%,<?php echo $color_gradient['to'] ?> 100%);
					background: linear-gradient(to bottom, <?php echo $color_gradient['from'] ?> 0%,<?php echo $color_gradient['to'] ?> 100%);
				}

				<?php elseif($boxed_background_type == '2'):
					$image_background = schon_get_option('body-background-image');
				?>
				body {
					background: url(<?php echo $image_background['background-image']; ?>) no-repeat center center fixed;
					-webkit-background-size: <?php echo $image_background['background-size']; ?>;
					-moz-background-size: <?php echo $image_background['background-size']; ?>;
					-o-background-size: <?php echo $image_background['background-size']; ?>;
					background-size: <?php echo $image_background['background-size']; ?>;
					background repeat: <?php echo $image_background['background-repeat']; ?>;
					background position: <?php echo $image_background['background-position']; ?>;
					background size: <?php echo $image_background['background-size']; ?>;
					background attachment: <?php echo $image_background['background-attachment']; ?>;
				}

				<?php elseif($boxed_background_type == '3'):
					$image_background = schon_get_option('body-background-pattern');
				?>
				body {
					background: url(<?php echo get_template_directory_uri() . "/admin/options/patterns/" . $image_background; ?>) repeat fixed;
				}

				<?php endif; ?>

			</style>
		<?php endif;
	}
}
add_action( 'wp_head', 'schon_style_boxed_layout', 997 );

if(!function_exists('schon_show_sidebar')) {
	function schon_show_sidebar( $name = null ) {

		/*
		$overrided_sidebar = schon_get_meta_option('schon_meta_overrided-sidebar');

		if(empty($overrided_sidebar) || $overrided_sidebar == "0")
			$sidebar = (is_null($name)) ? 'sidebar-page' : $name;
		else
			$sidebar = $overrided_sidebar;
		*/

		$sidebar = ( is_null( $name ) ) ? 'sidebar-page' : $name;

		$secondaryClass = schon_get_secondary_class( $sidebar );

		if ( ! is_active_sidebar( $sidebar ) || is_null( $secondaryClass ) ) {
			return;
		}
		?>


		<div id="secondary" class="widget-area <?php echo esc_attr($secondaryClass) ?>" role="complementary">
			<div class="secondary-heading"><a href="javascript:void(0)" id="close-sidebar-trigger"><i class="fa fa-times"></i> <?php esc_html_e('Close', 'schon'); ?></a></div>
			<?php dynamic_sidebar( $sidebar ); ?>
		</div><!-- #secondary -->
			<div class="sidebar-overlay"></div>

		<?php

	}
}


/************** WOOCOMMERCE ***************/
/*remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);*/
/*
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  //echo '<section id="main">';
  echo "<p> SONO QUI </p>";
}

function my_theme_wrapper_end() {
  //echo "<p> MI CHIUDO QUI </p>";
  echo '</section>';
}*/

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', 'schon_woocommerce_header_add_to_cart_fragment');
if(!function_exists('schon_woocommerce_header_add_to_cart_fragment')) {
	function schon_woocommerce_header_add_to_cart_fragment( $fragments ) {
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && schon_get_option( 'show-cart-primary-navigation' ) ):
			global $woocommerce;
			$products_number = $woocommerce->cart->cart_contents_count;
			ob_clean();
			?>

			<div class="mini-cart-header primary-menu-icon widget_shopping_cart">
				<div class="mini-cart-icon">
					<a class="cart-contents" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>"
					   title="<?php esc_html_e( 'View your shopping cart', 'woothemes' ); ?>">
						<i class="icon icon-handbag"></i>
						<span><?php echo $products_number; ?></span>
					</a>
				</div>
				<div class="inner <?php if ( $products_number == 0 )
					echo 'cart-empty' ?>">
					<div class="minicart-heading"><a href="javascript:void(0)" id="close-minicart-trigger"><i class="fa fa-times"></i> <?php esc_html_e('Close', 'schon'); ?></a></div>
					<?php woocommerce_mini_cart() ?>
				</div>

				<div class="mini-cart-overlay"></div>

				<script>
					'use strict';

					var $ = jQuery.noConflict();


					if ($('.mini-cart-header').length > 0) {
						$(".mini-cart-header .inner").mCustomScrollbar();

						$('.mini-cart-header').on('mouseenter', function () {
							$(".mini-cart-header .inner").mCustomScrollbar('update');
						});
					}

				</script>
			</div>
		<?php endif; ?>


		<?php
		$fragments['.mini-cart-header'] = ob_get_clean();

		return $fragments;
	}
}



if( ! function_exists( 'schon_page_title_bar' ) ) {
	function schon_page_title_bar( $title, $subtitle ) {



		$content_type = 'none';
		$extra_classes = '';

		/*
		if( strpos( $secondary_content, 'searchform' ) !== false ) {
			$content_type = 'search';
		} elseif ( $secondary_content != '' ) {
			$content_type = 'breadcrumbs';
		}
		*/

		$alignment = schon_get_option('page-title-bar-alignment');
		$background_url = schon_get_titlebar_background();
		$background_image = (empty($background_url)) ? '' : 'background-image:url("'.esc_url($background_url).'");';

		$color_style = schon_get_option('titlebar_color_style');


		?>

			<div id="page-title-bar" class="page-title-bar-content-<?php echo esc_attr($content_type) . ' ' . esc_attr($alignment) . ' ' . esc_attr($color_style); ?>" style="<?php echo esc_attr($background_image); ?>">
				<div class="titlebar-overlay"></div>
				<div class="container">
					<?php if( $title ): ?>
						<h1 class="entry-title"><?php echo esc_html($title); ?></h1>
						<?php if( $subtitle ): ?>
						<div class="h4 entry-subtitle"><?php echo esc_html($subtitle); ?></div>
						<?php endif; ?>
					<?php endif; ?>

					<?php schon_display_breadcrumbs(); ?>
				</div>
			</div>
		<?php
	}
}


if( ! function_exists( 'schon_current_page_title' ) ) {
	function schon_current_page_title(  ) {

		$title_bar_enabled = false;

		if( is_product()) {
			$title_bar_enabled = (bool)schon_get_option('show-titlebar-single-product');
		} else if( is_single() ) {
			$title_bar_enabled = (bool)schon_get_option('show-titlebar-single-post');
		} else if( !is_front_page()) {
			$title_bar_enabled = (bool)schon_get_option('show-titlebar');
		}

		$titlebar_hided_meta = (bool)schon_get_meta_option('schon_meta_hide-page-title-bar');
		if( $titlebar_hided_meta )
			$title_bar_enabled = false;

		if( !$title_bar_enabled )
			return

		/*
		//Used to get the meta of some particular pages? It need workaorund and tests
		$object_id = get_queried_object_id();
		if((get_option('show_on_front') && get_option('page_for_posts') && is_home()) ||
		   (get_option('page_for_posts') && is_archive() && !is_post_type_archive()) && !(is_tax('product_cat') ||
		                                                                                  is_tax('product_tag')) ||
		   (get_option('page_for_posts') && is_search())) {
			$post_id = get_option('page_for_posts');
		} else {
			if(isset($object_id)) {
				$post_id = $object_id;
			}

			if(class_exists('Woocommerce')) {
				if(is_shop()) {
					$post_id = get_option('woocommerce_shop_page_id');
				}
			}
		}
		*/

		$title = '';
		$subtitle = '';

		$subtitle = schon_get_meta_option('schon_meta_page-subtitle');
		if ( ! $subtitle && is_home()) {
			$subtitle = schon_get_option('blog-subtitle');
		}


		$title = get_the_title();

		if( is_home() ) {
			//$title = get_the_title( get_option( 'page_for_posts' ));
			$title = schon_get_option('blog-title');
		}

		if( is_search() ) {
			$title = esc_html__('Search results for:', 'schon') . get_search_query();
		}

		if( is_404() ) {
			$title = esc_html__('Error 404 Page', 'schon');
		}


		if( ( class_exists( 'TribeEvents' ) && tribe_is_event() && ! is_single() && ! is_home() ) ||
			( class_exists( 'TribeEvents' ) && is_events_archive() ) ||
			( class_exists( 'TribeEvents' ) && is_events_archive() && is_404() )
		) {
			$title = tribe_get_events_title();
		}


		if( is_archive() && ! is_bbpress() ) {
			if ( is_day() ) {
				$title = esc_html__( 'Daily Archives:', 'schon' ) . '<span> ' . get_the_date() . '</span>';
			} else if ( is_month() ) {
				$title = esc_html__( 'Monthly Archives:', 'schon' ) . '<span> ' . get_the_date( _x( 'F Y', 'monthly archives date format', 'schon' ) ) . '</span>';
			} elseif ( is_year() ) {
				$title = esc_html__( 'Yearly Archives:', 'schon' ) . '<span> ' . get_the_date( _x( 'Y', 'yearly archives date format', 'schon' ) ) . '</span>';
			} elseif ( is_author() ) {
				$curauth = get_user_by( 'id', get_query_var( 'author' ) );
				$title = $curauth->nickname;
			} elseif( is_post_type_archive() ) {
				$title = post_type_archive_title( '', false );

				$sermon_settings = get_option('wpfc_options');
				if( is_array( $sermon_settings ) ) {
					$title = $sermon_settings['archive_title'];
				}
			} else {
				$title = single_cat_title( '', false );
			}
		}

		//If class_exist( 'Woocommerce' ) && ..
		if( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_woocommerce() && ( is_product() || is_shop() ) && ! is_search() ) {
			if( ! is_product() ) {
				$title = woocommerce_page_title( false );
			}
		}

		schon_page_title_bar( $title, $subtitle );

		/*if( ! is_archive() && ! is_search() && ! ( is_home() && ! is_front_page() ) ) {
			//if( get_post_meta( $post_id, 'pyre_page_title', true ) == 'yes' ||
			//	( $smof_data['page_title_bar'] && get_post_meta( $post_id, 'pyre_page_title', true ) != 'no' )
			//) {

			/*
				if( get_post_meta( $post_id, 'pyre_page_title_text', true ) == 'no' ) {
					$title = '';
					$subtitle = '';
				}
			**
				//if( is_home() && is_front_page() && ! $smof_data['blog_show_page_title_bar'] )
				if( is_home() && is_front_page() && ! true ) {
					// do nothing
				} else {
					schon_page_title_bar( $title, $subtitle );
				}
			//}
		} else {

			//if is home e non mostrare il titolo nella pagina (! $smof_data['blog_show_page_title_bar'])
			if( is_home() && false ) {
				// do nothing
			} else {
				//If mostra il titolo nella pagina $smof_data['page_title_bar']
				if( true ) {
					schon_page_title_bar( $title, $subtitle );
				}
			}
		}*/

	}
}

if(!function_exists('schon_display_breadcrumbs')):
	function schon_display_breadcrumbs() {

		$breadcrumb_enabled = schon_get_option('show-breadcrumb');

		if(is_front_page() || !$breadcrumb_enabled) {
			return;
		}

		echo '<div id="breadcrumb-wrap">';

		if(is_woocommerce() || is_product()) {
			woocommerce_breadcrumb(array(
				'delimiter' => '',
				'wrap_before' => '<nav class="woocommerce-breadcrumb breadcrumb" itemprop="breadcrumb"><ol>',
				'wrap_after' => '</ol></nav>',
				'before' => '<li>',
				'after' => '</li>',
			));
			echo "</div>";
			return;
		}

		global $post;
		echo '<nav class="breadcrumb"><ol>';

		if ( !is_front_page() ) {
			echo '<li><a href="';
			echo esc_url(home_url('/'));
			echo '">'.esc_html__('Home', 'schon');
			echo "</a></li>";
		}

		$params['link_none'] = '';
		$separator = '';

		//if (is_category() && !is_singular('portfolio_type')) {
		if (is_category()) {
			/*$category = get_the_category();
			$ID = $category[0]->cat_ID;
			echo is_wp_error( $cat_parents = get_category_parents($ID, TRUE, '', FALSE ) ) ? '' : '<li>'.$cat_parents.'</li>';
			*/
			echo "<li>".single_cat_title("",false)."</li>";
		}

		if(is_singular('event')) {
			$terms = get_the_term_list($post->ID, 'event-categories', '<li>', '&nbsp;/&nbsp;', '</li>');
			if( ! is_wp_error( $terms ) ) {
				echo get_the_term_list($post->ID, 'event-categories', '<li>', '&nbsp;/&nbsp;', '</li>');
			}
			echo '<li>'.get_the_title().'</li>';
		}

		if (is_tax()) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$link = get_term_link( $term );

			if ( is_wp_error( $link ) ) {
				echo sprintf('<li>%s</li>', $term->name );
			} else {
				echo sprintf('<li><a href="%s" title="%s">%s</a></li>', esc_url($link), $term->name, $term->name );
			}
		}

		if(is_home()) {
			echo '<li>'.esc_html(schon_get_option('blog-title')).'</li>';
		}


		if(is_page() && !is_front_page()) {
			$parents = array();
			$parent_id = $post->post_parent;
			while ( $parent_id ) :
				$page = get_post( $parent_id );
				if ( $params["link_none"] )
					$parents[]  = get_the_title( $page->ID );
				else
					$parents[]  = '<li><a href="' . esc_url(get_permalink( $page->ID )) . '" title="' . get_the_title( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a></li>' . $separator;
				$parent_id  = $page->post_parent;
			endwhile;
			$parents = array_reverse( $parents );
			echo join( '', $parents );
			echo '<li>'.get_the_title().'</li>';
		}

		if(is_single() ) {
			$categories_1 = get_the_category($post->ID);
			if($categories_1):
				$cat_1_ids = array();
				foreach($categories_1 as $cat_1):
					$cat_1_ids[] = $cat_1->term_id;
				endforeach;
				$cat_1_line = implode(',', $cat_1_ids);
			endif;
			if( isset( $cat_1_line ) && $cat_1_line ) {
				$categories = get_categories(array(
					'include' => $cat_1_line,
					'orderby' => 'id'
				));
				if ( $categories ) :
					$cats = array();
					foreach ( $categories as $cat ) :
						$cats[] = '<li><a href="' . esc_url(get_category_link( $cat->term_id )) . '" title="' . $cat->name . '">' . $cat->name . '</a></li>';
					endforeach;
					echo join( '', $cats );
				endif;
			}
			echo '<li>'.get_the_title().'</li>';
		}

		if( is_tag() ){
			echo '<li>'."Tag: ".single_tag_title('',FALSE).'</li>';
		}
		if( is_search() ){
			echo '<li>'.esc_html__("Search", 'schon').'</li>';
		}
		if( is_year() ){
			echo '<li>'.get_the_time('Y').'</li>';
		}

		if( is_404() ) {
			echo '<li>'.esc_html__("404", 'schon'). " - " . esc_html__(schon_get_option('error404-content-title', esc_html__('Page not found', 'schon'))).'</li>';
		}


		if( is_archive() && is_post_type_archive() ) {
			$title = post_type_archive_title( '', false );

			echo '<li>'. $title .'</li>';
		}

		echo "</ol></nav></div>";
	}
endif;

if(!class_exists('schon_Submenu_Wrap')) {
	class schon_Submenu_Wrap extends Walker_Nav_Menu {
		function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "\n$indent<div class=\"sub-menu-wrap\"><div class=\"sub-menu-wrap-arrow\"></div><ul class=\"sub-menu\">\n";
		}

		function end_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "$indent</ul></div><div class='submenu-arrow'><i class=\"fa fa-chevron-right\"></i></div>\n";
		}

		public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;

			$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $class_names .'>';

			$atts = array();
			$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
			$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
			$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
			$atts['href']   = ! empty( $item->url )        ? $item->url        : '';

			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}

			/** This filter is documented in wp-includes/post-template.php */
			$title = apply_filters( 'the_title', $item->title, $item->ID );

			$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

			$image_tag = (in_array('image-column',$classes)) ? '<img src="'.$item->description.'" alt=" "/>' : '';
			$megamenu_column = (!in_array('remove-link',$classes));


			$item_output = $args->before;
			$item_output .= ($megamenu_column) ? '<a'. $attributes .'>' : '';
			$item_output .= $args->link_before . $title . $args->link_after;
			$item_output .= $image_tag;
			$item_output .= ($megamenu_column) ? '</a>' : '';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}
}

if(!function_exists('schon_footer_columns_layout')) {
	/**
	 * Get the footer column settings of columns from the db and write the script that handle the layout
	 */
	function schon_footer_columns_layout() {
		$row_1 = schon_get_option( 'footer-widget-row-1', '3-3-3-3' );
		$row_2 = schon_get_option( 'footer-widget-row-2', '3-3-3-3' );

		echo '
	<script>
	"use strict";
	if(window.matchMedia("(min-width: 992px)").matches) {
		var $ = jQuery;
		var layout_1 = "' . $row_1 . '",
		layout_2 = "' . $row_2 . '";
		layout_1 = layout_1.split("-");
		layout_2 = layout_2.split("-");
		
		if($("#footer-widgets-row-1").length > 0) {
			$("#footer-widgets-row-1 .footer-widget-column").each(function(i){
				var layout_length = layout_1.length; 
				
				$(this).removeClass("col-md-3");
				$(this).addClass("col-md-" + layout_1[i%layout_length]);
			});	
			
		}
		
		if($("#footer-widgets-row-1").length > 0) {
			$("#footer-widgets-row-2 .footer-widget-column").each(function(i){
				var layout_length = layout_2.length; 
				
				$(this).removeClass("col-md-3");
				$(this).addClass("col-md-" + layout_2[i%layout_length]);
			});	
						
		}
	}
	
	</script>';
	}
}
add_action('wp_footer', 'schon_footer_columns_layout');



/*--------------------------------------------------------------
 *********************** WOOCOMMERCE ***************************
--------------------------------------------------------------*/
if(!function_exists('schon_woocommerce_show_page_title')) {
	/**
	 * Remove the default Woocommerce title from Woocommerce pages
	 *
	 * @return bool
	 */
	function schon_woocommerce_show_page_title() {
		return false;
	}
}
add_filter('woocommerce_show_page_title', 'schon_woocommerce_show_page_title');

/* Woocommerce - Loop */
if(class_exists('WC_List_Grid')) {
	if(!function_exists('schon_relocate_grid_list_switch')) {
		/**
		 * Change the position of the switch Grid/List
		 */
		function schon_relocate_grid_list_switch() {
			global $WC_List_Grid;
			remove_action( 'woocommerce_before_shop_loop', array( $WC_List_Grid, 'gridlist_toggle_button' ), 30 );
			add_action( 'woocommerce_before_shop_loop', array( $WC_List_Grid, 'gridlist_toggle_button' ), 25 );
		}
	}
	add_action( 'wp', 'schon_relocate_grid_list_switch', 35);
}

if(!function_exists('schon_woocommerce_template_loop_product_link_open')) {
	/**
	 * Change the wrap of the thumbnail
	 */
	function schon_woocommerce_template_loop_product_link_open() {
		echo '<div class="woocommerce-product-image-container"><a class="woocommerce-product-image-wrap woocommerce-LoopProduct-link" href="' . get_the_permalink() . '">';
	}
}
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'schon_woocommerce_template_loop_product_link_open', 10 );



if(!function_exists('schon_woocommerce_template_loop_product_thumbnail')) {
	/**
	 * Move the opening of the link to open the product below the thumbnail
	 */
	function schon_woocommerce_template_loop_product_thumbnail() {

		global $product;
		$c = 1;

		$quickview_button = '';
		if ( schon_is_quickview_enabled() && schon_get_option( 'show-quickview-button-loop' ) ) {
			$c ++;

			//$quickview_button = '<a href="#" class="button yith-wcqv-button quickview" data-product_id="' . $product->id . '"><i class="icon icon-eye"></i></a>';
			$quickview_button = '<a href="#" class="button yith-wcqv-button quickview" data-product_id="' . $product->id . '"></a>';
		}

		$wishlist_button = '';
		if ( schon_is_wishlist_enabled() && schon_get_option( 'show-wishlist-button-loop' ) ) {
			$c ++;

			$add_to_wishlist = schon_get_add_to_wishlist_url( $product->id );

			$wishlist_button = '<a class="button add-to-wishlist ' . $add_to_wishlist['wishlist_class'] . '" href="' . $add_to_wishlist['wishlist_url'] . '"></a>';
		}

		$compare_button = '';
		if ( schon_is_compare_enabled() && schon_get_option( 'show-compare-button-loop' ) ) {
			$c ++;

			$add_to_compare = schon_get_add_to_compare_url( $product->id );

			$compare_button = '<a data-product_id="' . $product->id . '" class="button add-to-compare compare ' . $add_to_compare['compare_class'] . '" href="' . $add_to_compare['compare_url'] . '"></a>';
		}

		$additional_classes = 'buttons-' . $c;

		echo '</a><!-- /.woocommerce-product-image-wrap--><div class="add-to-cart-buttons-container ' . $additional_classes . '">';
		woocommerce_template_loop_add_to_cart();

		echo $quickview_button . $wishlist_button . $compare_button . '</div>';

		$align_text = schon_get_option('product-align-text');
		echo '</div><!-- add-to-cart-buttons-container --><div class="product-link-decription-container '.$align_text.'"><a href="' . get_the_permalink() . '" class="woocommerce-LoopProduct-link">';
	}
}
add_action( 'woocommerce_before_shop_loop_item_title', 'schon_woocommerce_template_loop_product_thumbnail', 10 );


function schon_woocommerce_shop_loop_item_title(){
	if(schon_get_option('show-titlebar-single-product')) {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	}
}
add_action( 'woocommerce_single_product_summary', 'schon_woocommerce_shop_loop_item_title',1 );

if(!function_exists('schon_woocommerce_template_loop_product_link_close_wrap')) {
	function schon_woocommerce_template_loop_product_link_close_wrap() {
		echo '</div><!-- /.product-link-decription-container-->';
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'schon_woocommerce_template_loop_product_link_close_wrap', 6 );

//if(class_exists('wcva_shop_page_swatches')){
//	remove_action('woocommerce_after_shop_loop_item_title', array('wcva_shop_page_swatches', 'wcva_change_shop_attribute_swatches'));)
//	add_action('woocommerce_after_shop_loop_item_title', array('wcva_shop_page_swatches', 'wcva_change_shop_attribute_swatches'));)
//}

if(!function_exists('schon_add_class_add_to_cart_buton')) {
	function schon_add_class_add_to_cart_buton( $args ) {
		$args['class'] = str_replace('button', '', $args['class']) . ' add-to-cart-button btn btn-primary';
		$args['class'] = str_replace('add_to_cart_ ', 'add_to_cart_button ', $args['class']);

		//$args['class'] = str_replace('ajax_add_to_cart', '', $args['class']);


		return $args;
	}
}
add_filter( 'woocommerce_loop_add_to_cart_args', 'schon_add_class_add_to_cart_buton' );

if(!function_exists('schon_add_buttons_woocommerce_list')) {
	/**
	 * Add custom class to add to cart buttons
	 */
	function schon_add_buttons_woocommerce_list() {
		global $product;

		if ( is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy() ) {
			if ( schon_is_wishlist_enabled() && schon_get_option( 'show-wishlist-button-loop' ) ) {

				$add_to_wishlist = schon_get_add_to_wishlist_url( $product->id );
				$label           = ( $add_to_wishlist['added'] ) ? esc_html__( 'View wishlist', 'schon' ) : esc_html__( 'Add to wishlist', 'schon' );

				echo '<a class="link add-to-wishlist ' . $add_to_wishlist['wishlist_class'] . '" href="' . $add_to_wishlist['wishlist_url'] . '"><i class="fa fa-heart-o"></i> ' . $label . '</a>';
			}

			if ( schon_is_compare_enabled() && schon_get_option( 'show-compare-button-loop' ) ) {

				$add_to_compare = schon_get_add_to_compare_url( $product->id );
				$label          = ( $add_to_compare['added'] ) ? esc_html__( 'View compare table', 'schon' ) : esc_html__( 'Compare', 'schon' );
				echo '<a data-product_id="' . $product->id . '" class="link add-to-compare compare ' . $add_to_compare['compare_class'] . '" href="' . $add_to_compare['compare_url'] . '"><i class="fa fa-exchange"></i> ' . $label . '</a>';
			}

			if ( schon_is_quickview_enabled() && schon_get_option( 'show-quickview-button-loop' ) ) {
				echo '<a href="#" class="link yith-wcqv-button quickview" data-product_id="' . $product->id . '"><i class="icon icon-eye"></i> ' . esc_html__( 'QuickView', 'schon' ) . '</a>';
			}
		}
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'schon_add_buttons_woocommerce_list', 11);

//Remove the button QuicView from the loop, added by default from the plugin
if(class_exists('YITH_WCQV_Frontend')) {
	$YITH_WCQV_Frontend = YITH_WCQV_Frontend();
	remove_action( 'woocommerce_after_shop_loop_item', array( $YITH_WCQV_Frontend, 'yith_add_quick_view_button' ), 15 );
}

if(!function_exists('schon_remove_add_to_cart_button')) {
	/**
	 * Remove the default buttons Add To Cart from the loop
	 */
	function schon_remove_add_to_cart_button() {
		if ( is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy() ) {
			return;
		}

		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	}
}
add_action( 'woocommerce_after_shop_loop_item', 'schon_remove_add_to_cart_button', 1);

if(!function_exists('schon_woocommerce_sale_flash')) {
	/**
	 * Calculate the discount of products on sale
	 *
	 * @param $value
	 *
	 * @return string
	 */
	function schon_woocommerce_sale_flash( $value ) {
		global $product;

		$sale_price    = $product->get_sale_price();
		$regular_price = $product->get_regular_price();

		if ( $regular_price != 0 ) {
			$percentage = 100 * ( 1 - ( $sale_price / $regular_price ) );
			$value      = sprintf( esc_html_x( '%s%% Off', '-15% Off', 'schon' ), intval( strval( $percentage ) ) );
		} else {
			$value = __( 'Sale!', 'woocommerce' );
		}

		return '<span class="onsale">' . $value . '</span>';
	}
}
add_filter('woocommerce_sale_flash', 'schon_woocommerce_sale_flash');

if(!function_exists('schon_woocommerce_template_summary_open_wrap')) {
	function schon_woocommerce_template_summary_open_wrap() {
		echo '<div class="summary-content">';
	}
}
add_action( 'woocommerce_single_product_summary', 'schon_woocommerce_template_summary_open_wrap', 1 );

if(!function_exists('schon_woocommerce_template_summary_close_wrap')) {
	function schon_woocommerce_template_summary_close_wrap() {
		echo '</div><!-- /.summary-content-->';
	}
}
add_action( 'woocommerce_single_product_summary', 'schon_woocommerce_template_summary_close_wrap', 99 );

//Remove the from the single product the Compare button added by default by plugin
if(schon_is_compare_enabled()) {
	global $yith_woocompare;
	remove_action( 'woocommerce_single_product_summary', array( $yith_woocompare->obj, 'add_compare_link' ), 35 );
}

if(!function_exists('schon_single_product_wishlist_link')) {
	/**
	 * Override the option of the plugin Wishlist and remove the button from single product
	 *
	 * @return string
	 */
	function schon_single_product_wishlist_link() {
		global $product;

		echo '<div class="single-product-wishlist-container">';

		if(schon_is_wishlist_enabled()) {
			$add_to_wishlist = schon_get_add_to_wishlist_url( $product->id );
			$label           = ( $add_to_wishlist['added'] ) ? esc_html__( 'View wishlist', 'schon' ) : esc_html__( 'Add to wishlist', 'schon' );

			echo '<a class="link add-to-wishlist ' . $add_to_wishlist['wishlist_class'] . '" href="' . $add_to_wishlist['wishlist_url'] . '"><i class="fa fa-heart-o"></i> ' . $label . '</a>';
		}

		if(schon_is_compare_enabled()) {
			$add_to_compare = schon_get_add_to_compare_url( $product->id );
			$label          = ( $add_to_compare['added'] ) ? esc_html__( 'View compare table', 'schon' ) : esc_html__( 'Compare', 'schon' );
			echo '<a data-product_id="' . $product->id . '" class="link add-to-compare compare ' . $add_to_compare['compare_class'] . '" href="' . $add_to_compare['compare_url'] . '"><i class="fa fa-exchange"></i> ' . $label . '</a>';
		}
		echo '</div>';
	}
}

//TODO Mettere questo dentro l'hook
if(schon_get_option('single_product_details_layout') == 'layout_2') {
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15 );
	add_action('woocommerce_single_product_summary', 'schon_single_product_wishlist_link', 21); //11
} else {
	add_action('woocommerce_single_product_summary', 'schon_single_product_wishlist_link', 11);
}

if(!function_exists('schon_override_billing_fields')) {
	/**
	 * Remove all label and place them values in the placeholder
	 *
	 * @param $fields
	 * @param $country
	 *
	 * @return array
	 */
	function schon_override_billing_fields( $fields) {

		foreach ( $fields as $key => $field ) {

			if ( isset( $field['required'] ) && $field['required'] == true && ! empty( $field['label'] ) ) {
				$fields[ $key ]['placeholder'] = $field['label'] . ' *';
			} else {
				$fields[ $key ]['placeholder'] = $field['label'];
			}

			$fields[ $key ]['label'] = '';
		}

		return $fields;
	}
}

//add_filter( 'woocommerce_billing_fields', 'schon_override_billing_fields', 10);
//add_filter( 'woocommerce_shipping_fields', 'schon_override_billing_fields', 10);
//add_filter( 'woocommerce_get_country_locale_default', 'schon_override_billing_fields', 10);

if(!function_exists('schon_render_steps_shop')) {
	/**
	 * Render the shopping steps in the pages Cart, Checkout and Order Received
	 */
	function schon_render_steps_shop() {

		$show_steps = schon_get_option( 'show-shop-steps' );
		if ( ! $show_steps ) {
			return;
		}

		$class_step_1 = ( is_cart() || is_checkout() || is_order_received_page() ) ? 'active' : '';
		$class_step_2 = ( is_checkout() || is_order_received_page() ) ? 'active' : '';
		$class_step_3 = ( is_order_received_page() ) ? 'active' : '';

		echo '<div id="schon-shop-steps-wrapper">
			<div id="schon-shop-steps-inner" class="clearfix">
				<div class="shop-step step-cart ' . $class_step_1 . '">
					<div class="step-number">' . esc_html__( '01', 'schon' ) . '</div>
					<div class="step-label">' . esc_html__( 'Shopping Cart', 'schon' ) . '</div>
				</div>
				<div class="shop-step shop-step-line"></div>
				<div class="shop-step step-checkout ' . $class_step_2 . '">
					<div class="step-number">' . esc_html__( '02', 'schon' ) . '</div>
					<div class="step-label">' . esc_html__( 'Checkout', 'schon' ) . '</div>
				</div>
				<div class="shop-step shop-step-line"></div>
				<div class="shop-step step-completed ' . $class_step_3 . '">
					<div class="step-number">' . esc_html__( '03', 'schon' ) . '</div>
					<div class="step-label">' . esc_html__( 'Finished', 'schon' ) . '</div>
				</div>
			</div>
		</div>';
	}
}
add_action( 'woocommerce_before_checkout_form', 'schon_render_steps_shop', 1 );
add_action( 'woocommerce_before_cart', 'schon_render_steps_shop', 1 );
add_action( 'schon_woocommerce_before_thankyou_success', 'schon_render_steps_shop', 1 );

if(!function_exists('schon_add_title_review_order_before_payment')) {
	function schon_add_title_review_order_before_payment() {
		echo '<h3>' . esc_html__( 'Payment Methods', 'schon' ) . '</h3>';
	}
}
add_action('woocommerce_review_order_before_payment','schon_add_title_review_order_before_payment', 1);

if(!function_exists('schon_woocommerce_before_account_navigation')) {
	function schon_woocommerce_before_account_navigation() {
		echo '<h3 class="woocommerce-MyAccount-navigation-title">' . esc_html__( 'My Account', 'schon' ) . '</h3>';
	}
}
add_action( 'woocommerce_before_account_navigation', 'schon_woocommerce_before_account_navigation' );

if(!function_exists('schon_woocommerce_order_button_html')) {
	/**
	 * Repalce the default classes from the button "Complete the order"
	 *
	 * @param $button_string
	 *
	 * @return mixed
	 */
	function schon_woocommerce_order_button_html( $button_string ) {
		return str_replace( 'button alt', 'btn btn-success btn-lg', $button_string );
	}
}
add_filter( 'woocommerce_order_button_html', 'schon_woocommerce_order_button_html' );


function schon_wcva_attribute_display_type() {
	return 'none';
}
if(class_exists('YITH_WCQV_Frontend')) {
	if(!function_exists('schon_layout_quickview')) {
		/**
		 * Override the ajax call of quickview in order to add a script
		 */
		function schon_layout_quickview() {
			if ( ! isset( $_REQUEST['product_id'] ) ) {
				die();
			}

			$product_id = intval( $_REQUEST['product_id'] );

			// set the main wp query for the product
			wp( 'p=' . $product_id . '&post_type=product' );

			// remove product thumbnails gallery
			//remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );

//			$wcva_shop_page_swatches = new wcva_shop_page_swatches();
//			$wcva_register_style_scripts = new wcva_register_style_scripts();
//
//			add_action( 'wp_enqueue_scripts', array(&$wcva_shop_page_swatches,'wcva_register_shop_scripts' ));
//			add_action( 'wp_enqueue_scripts', array(&$wcva_register_style_scripts,'wcva_register_my_scripts' ));

			add_filter('wcva_attribute_display_type', 'schon_wcva_attribute_display_type');

			// change template for variable products
			if ( isset( $GLOBALS['yith_wccl'] ) ) {
				$GLOBALS['yith_wccl']->obj = new YITH_WCCL_Frontend( YITH_WCCL_VERSION );
				$GLOBALS['yith_wccl']->obj->override();
			}

			ob_start();

			// load content template
			wc_get_template( 'yith-quick-view-content.php', array(), '', YITH_WCQV_DIR . 'templates/' );

			?>
			<script>
				'use strict';
				var $ = jQuery.noConflict();
				jQuery('#yith-quick-view-modal div.images img').on('load', function () {
					jQuery('#yith-quick-view-modal .summary').css('min-height', parseInt(jQuery('.yith-wcqv-main').outerHeight()));
				});

				var $product_thumbnails = $('.product .images .thumbnails');
				if ($product_thumbnails.length > 0) {
					$($product_thumbnails).owlCarousel({
						'pagination': false,
						'navigation': true,
						'afterAction': function () {
							var $next = $product_thumbnails.find('.owl-next'),
								$prev = $product_thumbnails.find('.owl-prev');
							if (this.itemsAmount > this.visibleItems.length) {
								$next.show();
								$prev.show();

								$next.removeClass('hidden');
								$prev.removeClass('hidden');
								if (this.currentItem == 0) {
									$prev.addClass('hidden');
								}
								if (this.currentItem == this.maximumItem) {
									$next.addClass('hidden');
								}
							}
						}
					});
				}

			</script>
			<?php
			echo ob_get_clean();

			die();
		}
	}

	$YITH_WCQV_Frontend = YITH_WCQV_Frontend();
	remove_action( 'wp_ajax_yith_load_product_quick_view', array( $YITH_WCQV_Frontend, 'yith_load_product_quick_view_ajax' ) );
	remove_action( 'wp_ajax_nopriv_yith_load_product_quick_view', array( $YITH_WCQV_Frontend, 'yith_load_product_quick_view_ajax' ) );

	add_action( 'wp_ajax_yith_load_product_quick_view', 'schon_layout_quickview' );
	add_action( 'wp_ajax_nopriv_yith_load_product_quick_view', 'schon_layout_quickview');
}

if(!function_exists('schon_remove_default_wishlist_button')) {
	/**
	 * Override the option of the plugin Wishlist and remove the button from single product
	 *
	 * @return string
	 */
	function schon_remove_default_wishlist_button() {
		return null;
	}
}
add_filter( 'yith_wcwl_positions', 'schon_remove_default_wishlist_button');

if (!function_exists('schon_loop_schon_columns')) {
	function schon_loop_schon_columns() {
		return schon_get_option('woocommerce-number-of-columns', 3);
	}
}
add_filter('loop_shop_columns', 'schon_loop_schon_columns');

if(!function_exists('schon_loop_shop_per_page')) {
	function schon_loop_shop_per_page() {
		return schon_get_option('woocommerce-number-of-products', 9);
	}
}
add_filter( 'loop_shop_per_page', 'schon_loop_shop_per_page', 20 );

if(!function_exists('schon_change_size_of_swatch_image')) {
	function schon_change_size_of_swatch_image($image_url, $image_id) {
		if(schon_get_option('woocommerce-number-of-columns') < 3)
			$size = 'shop_single';
		else
			$size = 'shop_catalog';
		$image_url = wp_get_attachment_image_src( $image_id, $size );
		return $image_url[0];
	}
}
add_filter('wcva_swatch_image_url','schon_change_size_of_swatch_image', 10 ,2);
add_filter('wcva_hover_image_url','schon_change_size_of_swatch_image', 10 ,2);

add_filter( 'woocommerce_output_related_products_args', 'schon_woocommerce_output_related_products_args' );
if(!function_exists('schon_woocommerce_output_related_products_args')) {
	function schon_woocommerce_output_related_products_args( $args ) {
		$args['posts_per_page'] = 4; // 4 related products
		$args['columns']        = 4; // arranged in 2 columns
		return $args;
	}
}

if(!function_exists('schon_single_product_archive_thumbnail_size')) {
	function schon_single_product_archive_thumbnail_size($image_size) {
		if(schon_get_option('woocommerce-number-of-columns') < 3)
			return 'shop_single';
		else
			return $image_size;
	}
}
add_filter('single_product_archive_thumbnail_size', 'schon_single_product_archive_thumbnail_size');
/* WOOCOMMERCE LOOP*/

// Catalog Mode
/**
 * Override the WooCcommerce function that render the price in order to check for the catalog mode
 */
function woocommerce_template_single_price() {

	if( schon_is_shopping_price_enabled() )
		wc_get_template( 'single-product/price.php' );
	
}

if ( !function_exists('schon_add_filters_accordion')) {
	/**
	 * Render the filters accordion section in Shop pages
	 */
	function schon_add_filters_accordion() {

		$filers_accordion_enabled = (bool) schon_get_option( 'woocommerce-filters-sidebar-enabled', false );
		if ( ! $filers_accordion_enabled )
			return;

		$catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array(
			'menu_order' => __( 'Default sorting', 'woocommerce' ),
			'popularity' => __( 'Sort by popularity', 'woocommerce' ),
			'rating'     => __( 'Sort by average rating', 'woocommerce' ),
			'date'       => __( 'Sort by newness', 'woocommerce' ),
			'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
			'price-desc' => __( 'Sort by price: high to low', 'woocommerce' )
		) );

		$link = schon_remove_woocommerce_query_arg( 'orderby' );

		if ( strpos( $link, '?' ) !== false ) {
			$connector = '&';
		} else {
			$connector = '?';
		}
		?>

		<?php
		// Active filters
		$is_valid_orderby = ( isset( $_GET['orderby'] ) && array_key_exists( $_GET['orderby'], $catalog_orderby_options ) );
		echo '<div id="shon-shop_active-filters" class="widget_layered_nav_filters clear"><ul>';
		if ( $is_valid_orderby ) {
			$remove_orderby_link = $link;
			echo '<li><a title="' . esc_html__( 'Remove filter', 'woocommerce' ) . '" href="' . $remove_orderby_link . '">' . $catalog_orderby_options[ $_GET['orderby'] ] . '</a></li>';
		}
		the_widget( 'WC_Widget_Layered_Nav_Filters', '', 'before_widget=<li>&after_widget=</li>' );
		echo '</ul></div>';
		?>

		<div id="schon-shop_filters-accordion-wrap" class="collapse">
			<div id="schon-shop_filters-accordion-inner" class="clear">
				<div class="widget">
					<h3 class="widget-title"><?php esc_html_e( 'Sort by', 'schon' ); ?></h3>
					<ul>
						<?php
						$current_sorting = ( $is_valid_orderby ) ? $_GET['orderby'] : 'menu_order';
						foreach ( $catalog_orderby_options as $id => $name ) {
							$active_class = ( $current_sorting == $id ) ? 'class="active"' : '';
							echo '<li><a ' . $active_class . ' href="' . $link . $connector . 'orderby=' . $id . '" >' . $name . '</a></li>';
						}
						?>
					</ul>
				</div>
				<?php dynamic_sidebar( 'sidebar-woocommerce-filters' ); ?>
			</div>
		</div>
		<div class="margin-20"></div>
		<?php
	}
}
add_action('woocommerce_before_shop_loop', 'schon_add_filters_accordion', 40);