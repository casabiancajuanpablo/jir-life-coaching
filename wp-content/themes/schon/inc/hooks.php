<?php

if ( ! function_exists( 'schon_setup' ) ) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function schon_setup()
    {


        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-slider');

	    if (apply_filters( 'schon_woocommerce_gallery_zoom_enabled', true))
            add_theme_support('wc-product-gallery-zoom');

	    if (apply_filters( 'schon_woocommerce_gallery_lightbox_enabled', true))
            add_theme_support('wc-product-gallery-lightbox');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Main Menu', 'schon'),
	        'actionbar-loggedin' => esc_html__('Actionbar (Logged In)', 'schon'),
	        'actionbar-loggedout' => esc_html__('Actionbar (Logged Out)', 'schon')
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See http://codex.wordpress.org/Post_Formats
         */
        /*add_theme_support('post-formats', array(
            'aside', 'image', 'video', 'quote', 'link',
        ));*/

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('schon_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

	    add_filter('widget_text', 'do_shortcode');
    }
} // schon_setup
add_action( 'after_setup_theme', 'schon_setup' );



if(!function_exists('schon_widgets_init')) {
    /**
     * Register widget area.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_sidebar
     */
    function schon_widgets_init()
    {
        register_sidebar(array(
            'name' => esc_html__('Pages', 'schon'),
            'id' => 'sidebar-page',
            'description' => '',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));
        register_sidebar(array(
            'name' => esc_html__('Blog Pages', 'schon'),
            'id' => 'sidebar-blog',
            'description' => '',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        //Woocommerce
        register_sidebar(array(
            'name' => esc_html__('Woocommerce Shop Pages', 'schon'),
            'id' => 'sidebar-woocommerce',
            'description' => '',
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

	    //Woocommerce Filters
	    register_sidebar(array(
		    'name' => esc_html__('Woocommerce Filters Accordion', 'schon'),
		    'id' => 'sidebar-woocommerce-filters',
		    'description' => '',
		    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		    'after_widget' => '</aside>',
		    'before_title' => '<h3 class="widget-title">',
		    'after_title' => '</h3>',
	    ));

	    register_sidebar(array(
		    'name' => esc_html__('Footer Row 1', 'schon'),
		    'id' => 'footer-row-1',
		    'before_widget' => '<div class="footer-widget-column col-md-3 col-sm-6"><aside id="%1$s" class="widget %2$s">',
		    'after_widget' => '</aside></div>',
		    'before_title' => '<h3 class="widget-title">',
		    'after_title' => '</h3>',
	    ));
	    register_sidebar(array(
		    'name' => esc_html__('Footer Row 2', 'schon'),
		    'id' => 'footer-row-2',
		    'before_widget' => '<div class="footer-widget-column col-md-3 col-sm-6"><aside id="%1$s" class="widget %2$s">',
		    'after_widget' => '</aside></div>',
		    'before_title' => '<h3 class="widget-title">',
		    'after_title' => '</h3>',
	    ));

    }
}
add_action( 'widgets_init', 'schon_widgets_init' );



if(!function_exists('schon_scripts')) {
    /**
     * Enqueue scripts and styles.
     */
    function schon_scripts()
    {
	    //Todo pulire i "css" e style e Script

        wp_enqueue_style('bootstrap', get_template_directory_uri() . "/lib/bootstrap/css/bootstrap.min.css");

	    //Icons
        wp_enqueue_style("font-awesome", get_template_directory_uri() . "/lib/font-awesome/css/font-awesome.min.css");
        wp_enqueue_style("simple-line-icons", get_template_directory_uri() . "/lib/simple-line-icons/css/simple-line-icons.css");

	    wp_enqueue_style("custom-scrollbar", get_template_directory_uri() . "/lib/m-custom-scrollbar/jquery.mCustomScrollbar.min.css");

	    //Owl Carousel
	    wp_enqueue_style("owl-carousel", get_template_directory_uri() . "/lib/owl-carousel/owl.carousel.css");
	    wp_enqueue_style("owl-carousel-theme", get_template_directory_uri() . "/lib/owl-carousel/owl.theme.css");
	    wp_enqueue_style("owl-carousel-transitions", get_template_directory_uri() . "/lib/owl-carousel/owl.transitions.css");

		//Schon Style
	    wp_enqueue_style('schon', get_stylesheet_uri());

	    //Theme Options
        wp_enqueue_style('theme-options', get_template_directory_uri() . "/css/theme-options.css", array('schon'));


        wp_enqueue_script('jquery');

        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/lib/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.2', true);

        wp_enqueue_script('schon-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);

        wp_enqueue_script('custom-scrollbar', get_template_directory_uri() . '/lib/m-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), false, true);

	    //Owl Carousel
	    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/lib/owl-carousel/owl.carousel.min.js', array('jquery'), false, true);

	    //Isotope
	    wp_enqueue_script('isotope', get_template_directory_uri() . '/lib/isotope/isotope.pkgd.min.js', array('jquery'), false, true);

	    //Schon Scripts
	    wp_enqueue_script('schon', get_template_directory_uri() . '/js/scripts.js', array('jquery', 'jquery-ui-accordion', 'isotope'), false, true);


        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

	    if(schon_is_compare_enabled()) {
		    global $yith_woocompare;
		    wp_localize_script( 'yith-woocompare-main', 'yith_woocompare', array(
			    'ajaxurl'   => version_compare( WC()->version, '2.4', '>=' ) ? WC_AJAX::get_endpoint( "%%endpoint%%" ) : admin_url( 'admin-ajax.php', 'relative' ),
			    'actionadd' => $yith_woocompare->obj->action_add,
			    'actionremove' => $yith_woocompare->obj->action_remove,
			    'actionview' => $yith_woocompare->obj->action_view,
			    'added_label' => '',
			    'table_title' => __( 'Product Comparison', 'yith-woocommerce-compare' ),
			    'auto_open' => get_option( 'yith_woocompare_auto_open', 'yes' ),
			    'loader'    => YITH_WOOCOMPARE_ASSETS_URL . '/images/loader.gif',
			    'button_text' => get_option('yith_woocompare_button_text')
		    ));
	    }

    }
}
add_action( 'wp_enqueue_scripts', 'schon_scripts', 50);


if(!function_exists('load_custom_wp_admin_style')) {
    function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/admin-style.css' );
        wp_enqueue_style( 'custom_wp_admin_css' );
    }
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


if(!function_exists('schon_plugins_loaded')) {
    /**
     * Register default function when plugin not activated
     */
    function schon_plugins_loaded()
    {
        if (!function_exists('is_woocommerce')) {
            function is_woocommerce()
            {
                return false;
            }
        }
	    if (!function_exists('is_product')) {
		    function is_product()
		    {
			    return false;
		    }
	    }
        if (!function_exists('is_bbpress')) {
            function is_bbpress()
            {
                return false;
            }
        }
        if (!function_exists('bbp_is_forum_archive')) {
            function bbp_is_forum_archive()
            {
                return false;
            }
        }
        if (!function_exists('bbp_is_topic_archive')) {
            function bbp_is_topic_archive()
            {
                return false;
            }
        }
        if (!function_exists('bbp_is_user_home')) {
            function bbp_is_user_home()
            {
                return false;
            }
        }
        if (!function_exists('bbp_is_search')) {
            function bbp_is_search()
            {
                return false;
            }
        }

        if (!function_exists('tribe_is_event')) {
            function tribe_is_event()
            {
                return false;
            }
        }
        function is_events_archive()
        {
            if (class_exists('TribeEvents')) {
                if (tribe_is_month() ||
                    tribe_is_day() ||
                    tribe_is_past() ||
                    tribe_is_upcoming() ||
                    class_exists('TribeEventsPro') && (tribe_is_week() || tribe_is_photo() || tribe_is_map())
                ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}
add_action( 'wp_head', 'schon_plugins_loaded' );

/**
 * Flush out the transients used in schon_categorized_blog.
 */
function schon_category_transient_flusher() {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient( 'schon_categories' );
}
add_action( 'edit_category', 'schon_category_transient_flusher' );
add_action( 'save_post',     'schon_category_transient_flusher' );


if(!function_exists('schon_add_overlapping_header_class')) {
	/**
	 * Add to the header the class that enable or disable the header overlapping
	 *
	 * @param $classes
	 *
	 * @return string
	 */
	function schon_add_overlapping_header_class($classes) {

		$overlapping_header = schon_get_option('overlapping-header');

		$override_overlapping_header = schon_get_meta_option('schon_meta_override-overlapping-header', 0);

		if($override_overlapping_header != 0)
			$overlapping_header = ($override_overlapping_header == 1) ? true : false;


		if($overlapping_header)
			$classes .= ' overlapping-header';

		return $classes;
	}
}
add_filter('schon_masthead_classes', 'schon_add_overlapping_header_class');

if(!function_exists('schon_override_titlebar_padding')) {
	//Todo, change this, add a filter that add the style inline?
	/**
	 * Override the titlebar padding writing css on the page
	 */
	function schon_override_titlebar_padding() {

		$overlapping_header = schon_get_option('overlapping-header');

		$titlebar_padding_top = schon_get_meta_option('schon_meta_titlebar-padding-top');
		$titlebar_padding_bottom = schon_get_meta_option('schon_meta_titlebar-padding-bottom');

		if($titlebar_padding_top != '' || $titlebar_padding_bottom != '') {
			echo '<style>';

			if($titlebar_padding_top != 0)
				echo '#page-title-bar { padding-top: ' .$titlebar_padding_top .'px; }';

			if($titlebar_padding_bottom != 0)
				echo '#page-title-bar { padding-bottom: ' .$titlebar_padding_bottom .'px; }';

			echo '</style>';
		}

	}
}
add_action('wp_footer', 'schon_override_titlebar_padding');

if(!function_exists('schon_main_classes_for_blog_layout')) {
	function schon_main_classes_for_blog_layout($classes_string) {
		if (is_home() || is_category()) {
			$classes_string .= ' ' . ( schon_get_option( 'blog-listing-layout' ) );
			$classes_string .= (schon_get_option('blog-date-as-badge')) ? ' blog-post-date-badge': '';
		}

		return $classes_string;
	}
}
add_filter('schon_main_classes', 'schon_main_classes_for_blog_layout');

// Catalog mode
if ( !function_exists('schon_check_catalog_mode_add_to_cart')) {
	function schon_check_catalog_mode_hooks() {
		if ( schon_is_catalog_mode_enabled() ) {
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
			remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
			remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );
			remove_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );
			remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
		}

		if ( !schon_is_shopping_price_enabled())
			remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	}
}
add_action( 'after_setup_theme', 'schon_check_catalog_mode_hooks');

if( !function_exists('schon_check_catalog_mode_redirect') ) {
	function schon_check_catalog_mode_redirect() {
		if ( ! schon_is_catalog_mode_enabled() )
			return;

		if( class_exists('Woocommerce') && ( is_cart() || is_checkout() || is_checkout_pay_page()) )
			wp_redirect(site_url());
	}
}
add_action( 'template_redirect', 'schon_check_catalog_mode_redirect');

function schon_remove_order_by_price( $params ) {

	if(!schon_is_shopping_price_enabled()) {
		unset( $params['price'] );
		unset( $params['price-desc'] );
	}

	return $params;
}
add_filter( 'woocommerce_catalog_orderby', 'schon_remove_order_by_price');