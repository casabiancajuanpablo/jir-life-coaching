<?php
if(!function_exists('schon_get_option')) {
	/**
	 * Get value of option passed by parameter
	 *
	 * @param string $optionId
	 * @param $default [null]
	 *
	 * @return
	 */
	function schon_get_option( $optionId, $default = null ) {
		global $schon_options;

		//if(is_null($default))
		//    return (array_key_exists($optionId, $schon_options)) ? $schon_options[$optionId] : $default;
		//else
		//    return (!is_null($schon_options[$optionId]) && !empty($schon_options[$optionId])) ? $schon_options[$optionId] : $default;

		if ( ! is_null( $schon_options ) ) {
			if ( array_key_exists( $optionId, $schon_options ) ) {
				if ( ! is_null( $default ) && ( is_null( $schon_options[ $optionId ] ) || empty( $schon_options[ $optionId ] ) ) ) {
					return $default;
				} else {
					return $schon_options[ $optionId ];
				}
			} else {
				return $default;
			}
		}

		return $default;

	}
}

if(!function_exists('schon_get_meta_option')) {
	/**
	 * Get value of meta option passed by parameter
	 *
	 * @param string $meta_id
	 * @param $default
	 * @param array|string $args default array()
	 *
	 * @return
	 */
	function schon_get_meta_option( $meta_id, $default = null, $args = array() ) {

		if ( function_exists( 'rwmb_meta' ) ) {
			$meta = rwmb_meta( $meta_id, $args );

			$meta = ( empty( $meta ) && ! is_null( $default ) ) ? $default : $meta;
		} else {
			$meta = $default;
		}

		return $meta;
	}
}

/**
 * Get value of meta option passed by parameter
 * @param string $meta_id
 * @param $default
 * @param array|string $args default array()
 * @return
 */
function schon_get_term_meta_option($meta_id, $default = null , $args = array()) {

	if ( function_exists( 'get_term_meta' ) ) {
		$meta = get_term_meta($meta_id, $args);

		$meta = (empty($meta) && !is_null($default)) ? $default : $meta;
	} else {
		$meta = $default;
	}

	return $meta;
}

if(!function_exists('schon_get_sidebar_option')) {
	/**
	 * Get the right sidebar option
	 *
	 * @param null|string $name
	 *
	 * @return int
	 */
	function schon_get_sidebar_option( $name = null) {

		/*if(empty($overrided_sidebar) || $overrided_sidebar == "0")
			$name = (is_null($name)) ? 'sidebar-page' : $name;
		else
			$name = $overrided_sidebar;*/

		if(is_null($name)) {
			if( is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy() || is_product())
				$name = 'sidebar-woocommerce';
			else if (is_home() || is_single() || is_category() || is_archive())
				$name = 'sidebar-blog';
			else
				$name = 'sidebar-page';
		}

		switch ($name) {
			case "sidebar-page":
			default:
				$sidebarOption = schon_get_option('default-layout');
//                $secondaryWidth = schon_get_option('sidebar-width');
				break;

			case "sidebar-blog":
				$sidebarOption = schon_get_option('blog-layout');
//                $secondaryWidth = schon_get_option('blog-sidebar-width');
				break;

			case "sidebar-woocommerce":
				if(is_product())
					$sidebarOption = schon_get_option('woocommerce-single-product-layout');
				else
					$sidebarOption = schon_get_option('woocommerce-layout');
//                $secondaryWidth = schon_get_option('woocommerce-sidebar-width');
				break;
		}

		$metaSidebar = schon_get_meta_option('schon_meta_override-sidebar-position');
		$sidebarOption = (!empty($metaSidebar)) ? $metaSidebar : $sidebarOption;

		return $sidebarOption;

	}
}

if(!function_exists('schon_get_primary_class')) {
    /**
     * Get primary column class
     * @param string name
     * @return string
     */
    function schon_get_primary_class($name = null)
    {

	    $sidebarOption = schon_get_sidebar_option( $name );

	    $secondaryWidth = 3;

        if ( $sidebarOption == 1 || $sidebarOption == 4)
            $primaryWidthClass = 'col-md-12';
        else
            $primaryWidthClass = 'col-sm-' . (12 - $secondaryWidth);

        if ($sidebarOption == 3)
            $primaryWidthClass .= ' col-sm-push-' . $secondaryWidth;

        return $primaryWidthClass;
    }
}

if(!function_exists('schon_get_secondary_class')) {
    /**
     * Get secondary column width
     * @param string $name
     * @return string|null
     */
    function schon_get_secondary_class($name = null)
    {

	    $sidebarOption = schon_get_sidebar_option( $name );

	    $secondaryWidth = 3;

        if ($sidebarOption == 1)
            $secondaryWidthClass = null;
        else
            $secondaryWidthClass = 'col-sm-' . $secondaryWidth;

        if ($sidebarOption == 3)
            $secondaryWidthClass .= ' col-sm-pull-' . (12 - $secondaryWidth);

        return $secondaryWidthClass;
    }
}

if(!function_exists('schon_static_blocks')) {

	/**
	 * Print a section of static blocks
	 * @param $section
	 * @param $class_wrapper
	 */
	function schon_static_blocks($section, $class_wrapper = '') {
		$actionbar_fields_left = schon_get_option($section);
		if(!empty($actionbar_fields_left)) {
			echo '<div class="staticblocks-section '.$class_wrapper.'">';
			foreach ( $actionbar_fields_left as $field ) {
				$no_margin_class = ($field == '[schon_loggedinout_menu]') ? 'no-margin' : '' ;
				echo '<div class="staticblock-field '.$no_margin_class.'">' . do_shortcode( $field ) . '</div>';
			}
			echo '</div>';
		}
	}
}

if(!function_exists('schon_get_rand_value')) {
	function schon_get_rand($n = 10){
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';

		$string = '';
		for ($i = 0; $i < $n; $i++) {
			$string .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		return $string;
	}
}


if(!function_exists('schon_is_wishlist_enabled')) {

	/**
	 * Check if the wishlist is is installed and enabled
	 * @return bool
	 */
	function schon_is_wishlist_enabled() {
		return (class_exists('YITH_WCWL') && get_option( 'yith_wcwl_enabled' ) == 'yes');
	}
}

if(!function_exists('schon_is_compare_enabled')) {

	/**
	 * Check if the compare is is installed and enabled
	 * @return bool
	 */
	function schon_is_compare_enabled() {
		return class_exists('YITH_Woocompare_Frontend');
	}
}

if(!function_exists('schon_is_quickview_enabled')) {

	/**
	 * Check if the quick view feature is is installed and enabled
	 * @return bool
	 */
	function schon_is_quickview_enabled() {
		return class_exists('YITH_WCQV_Frontend');
	}
}


if(!function_exists('schon_get_wishlist_url')) {

	/**
	 * Return the wishlist page url
	 * @return bool
	 */
	function schon_get_wishlist_url() {
		return (class_exists('YITH_WCWL')) ? YITH_WCWL()->get_wishlist_url() : '';
	}
}


if(!function_exists('schon_get_add_to_wishlist_url')) {

	/**
	 * Return the url and the class for the buttons/links "Add to wishlist"
	 * 
	 * @param int $product_id
	 *
	 * @return array
	 */
	function schon_get_add_to_wishlist_url($product_id) {

		if(!class_exists('YITH_WCWL'))
			return null;

		$added_to_wishlist = YITH_WCWL()->get_products(array('is_default' => true, 'product_id' => $product_id));

		if(!empty($added_to_wishlist)) {
			$wishlist_class = 'wishlist-added added';
			//$wishlist_url = esc_url( add_query_arg( 'remove_from_wishlist', $product_id ) );
			$wishlist_url = schon_get_wishlist_url();
			$added = true;
		} else {
			$wishlist_class = '';
			$wishlist_url = esc_url( add_query_arg( 'add_to_wishlist', $product_id ) );
			$added = false;
		}
		
		return array('wishlist_class' => $wishlist_class, 'wishlist_url' => $wishlist_url, 'added' => $added);
	}
}

if(!function_exists('schon_get_add_to_compare_url')) {

	/**
	 * Return the url and the class for the buttons/links "Add to compare"
	 *
	 * @param int $product_id
	 *
	 * @return array
	 */
	function schon_get_add_to_compare_url($product_id) {

		if(!class_exists('YITH_Woocompare_Frontend'))
			return null;

		global $yith_woocompare;
		$compare_products = $yith_woocompare->obj->get_products_list();

		$added_to_compare = false;
		foreach($compare_products as $struct) {
			if ($struct->id == $product_id) {
				$added_to_compare = true;
				break;
			}
		}

		/*
		if(empty($compare_products)) {
			$compare_url = $YITH_Woocompare_Frontend->add_product_url($product_id);
		} else {
			$compare_url = ($added_to_compare) ? $YITH_Woocompare_Frontend->remove_product_url($product_id) : $YITH_Woocompare_Frontend->add_product_url($product_id);
		}
		*/

		if($added_to_compare) {
			$compare_class = 'compare-added added';
			$added = true;
		} else {
			$compare_class = '';
			$added = false;
		}

		return array( 'compare_class' => $compare_class,  'compare_url' => $yith_woocompare->obj->add_product_url($product_id), 'added' => $added);
	}
}

if(!function_exists('schon_get_category_img_url')) {
	function schon_get_category_img_url() {
		if (function_exists('z_taxonomy_image_url'))
			return z_taxonomy_image_url();
		else
			return '';
	}
}

if(!function_exists('schon_get_titlebar_background')) {
	function schon_get_titlebar_background() {

		//if its a "single item".
		//Necessary until metabox.io don't avoid to get the first prost information on the taxconomy page
		if(is_single() || is_product() || is_page()) {
			$background = schon_get_meta_option( 'schon_meta_titlebar-background-image' );
			if ( ! empty( $background ) ) {
				$background = array_values( $background );
				$background = array_shift( $background );

				return $background['url'];
			}
		}

		//Get the taxonomy to use for the current post type
		$post_type = get_post_type();
		$taxonomy = '';
		switch($post_type) {
			case 'post':
				$taxonomy = 'category';
				break;
			case 'product':
				$taxonomy = 'product_cat';
				break;
		}

		if(!empty($taxonomy)) {
			global $post;

			if(is_single() || is_product()) {
				$categories = get_the_terms($post->ID, $taxonomy);
				$term_id = (!empty($categories)) ? $categories[count($categories)-1]->term_id : null;
			} else {
				$categories = get_term_by('term_name', get_queried_object_id(), $taxonomy);
				$term_id = (!empty($categories)) ? $categories->term_id : null;
			}


			if (function_exists('z_taxonomy_image_url') && !is_null($term_id)) {
				//$taxonomy_background = schon_get_term_meta_option('schon_meta_titlebar_background_image');
				$taxonomy_background = z_taxonomy_image_url($term_id);
				if(!empty($taxonomy_background))
					return $taxonomy_background;
			}

			//Todo: recursive in case of nested terms
		}


		$default_image = schon_get_option('titlebar-default-image');


		return $default_image['url'];


	}
}
if(!function_exists('schon_adjust_color')) {
	function schon_adjust_color( $hex, $steps ) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max( - 255, min( 255, $steps ) );

		// Normalize into a six character long hex string
		$hex = str_replace( '#', '', $hex );
		if ( strlen( $hex ) == 3 ) {
			$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );
		}

		// Split into three parts: R, G and B
		$color_parts = str_split( $hex, 2 );
		$return      = '#';

		foreach ( $color_parts as $color ) {
			$color = hexdec( $color ); // Convert to decimal
			$color = max( 0, min( 255, $color + ( $color * ( $steps / 255 ) ) ) ); // Adjust color
			$return .= str_pad( dechex( $color ), 2, '0', STR_PAD_LEFT ); // Make two char hex code
		}

		return $return;
	}
}

if ( !function_exists('schon_is_catalog_mode_enabled')) {
	/**
	 * Check if the catalog mode is enabled
	 */
	function schon_is_catalog_mode_enabled() {

		global $schon_is_catalog_mode_enabled;
		
		if( !is_null($schon_is_catalog_mode_enabled))
			return $schon_is_catalog_mode_enabled;
		
		return (bool)schon_get_option('catalog-mode-enabled',false);
		
	}
}

if ( !function_exists('schon_is_shopping_price_enabled')) {
	/**
	 * Check if the catalog mode is enabled
	 */
	function schon_is_shopping_price_enabled() {

		global $schon_is_price_enabled;

		if( !is_null($schon_is_price_enabled))
			return $schon_is_price_enabled;

		return !(schon_is_catalog_mode_enabled() && (bool)schon_get_option('shopping-price-disabled',false));

	}
}
if( !function_exists('schon_remove_woocommerce_query_arg')) {
	/**
	 * Get the current url or a given one and remove a woocommerce query arg from there
	 *
	 * @param $arg
	 * @param null $link
	 *
	 * @return false|null|string|void|WP_Error
	 */
	function schon_remove_woocommerce_query_arg( $arg, $link = null ) {

		if ( is_null( $link ) ) {
			if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
				$link = home_url();
			} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id( 'shop' ) ) ) {
				$link = get_post_type_archive_link( 'product' );
			} else {
				$link = get_term_link( get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			}
		}

		// Min/Max
		if ( isset( $_GET['min_price'] ) && $arg != 'min_price' ) {
			$link = add_query_arg( 'min_price', wc_clean( $_GET['min_price'] ), $link );
		}

		if ( isset( $_GET['max_price'] ) && $arg != 'max_price' ) {
			$link = add_query_arg( 'max_price', wc_clean( $_GET['max_price'] ), $link );
		}

		// Orderby
		if ( isset( $_GET['orderby'] ) && $arg != 'orderby' ) {
			$link = add_query_arg( 'orderby', wc_clean( $_GET['orderby'] ), $link );
		}

		/**
		 * Search Arg.
		 * To support quote characters, first they are decoded from &quot; entities, then URL encoded.
		 */
		if ( get_search_query() ) {
			$link = add_query_arg( 's', rawurlencode( htmlspecialchars_decode( get_search_query() ) ), $link );
		}

		// Post Type Arg
		if ( isset( $_GET['post_type'] ) ) {
			$link = add_query_arg( 'post_type', wc_clean( $_GET['post_type'] ), $link );
		}

		// All current filters
		if ( $_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes() ) {
			foreach ( $_chosen_attributes as $name => $data ) {
				$filter_name = sanitize_title( str_replace( 'pa_', '', $name ) );
				if ( ! empty( $data['terms'] ) ) {
					$link = add_query_arg( 'filter_' . $filter_name, implode( ',', $data['terms'] ), $link );
				}
				if ( 'or' == $data['query_type'] ) {
					$link = add_query_arg( 'query_type_' . $filter_name, 'or', $link );
				}
			}
		}

		return $link;
	}
}