<?php

add_filter( 'rwmb_meta_boxes', 'schon_register_meta_boxes' );

function schon_register_meta_boxes( $meta_boxes )
{
    $prefix = 'schon_meta_';

    // 1st meta box
    $meta_boxes[] = array(
        'id'       => 'page-layout',
        'title'    => 'Page Options',
        'pages'    => get_post_types(),
        'context'  => 'advanced',
        'priority' => 'high',

        'fields' => array(
/*
	        array(
		        'name'  => esc_html__('Use different sidebar options', 'schon'),
		        'id'    => $prefix . 'overrided-sidebar',
		        'desc'  => esc_html__('Use sidebar content and layout of another page type.', 'schon'),
		        'type'  => 'select',
		        'options'=> array(
			        '0' => esc_html__('Default', 'schon'),
			        'sidebar-page' => esc_html__('General Pages Sidebar', 'schon'),
			        'sidebar-blog' => esc_html__('Blog Sidebar', 'schon'),
			        'sidebar-woocommerce' => esc_html__('Woocommerce Sidebar', 'schon'),
		        )
	        ),*/

            array(
                'name'  => esc_html__('Override sidebar Options', 'schon'),
                'desc'  => esc_html__('Override the sidebar using a specific option', 'schon'),
                'id'    => $prefix . 'override-sidebar-position',
                'type'  => 'select',
                'options'=> array(
                				'0' => esc_html__('Not override', 'schon'),
                				'1' => esc_html__('No Sidebar', 'schon'),
                				'2' => esc_html__('Sidebar Right', 'schon'),
                				'3' => esc_html__('Sidebar Left', 'schon'),
                            )
            ),

	        array(
		        'name'  => esc_html__('Override Overlapping Header', 'schon'),
		        'id'    => $prefix . 'override-overlapping-header',
		        'type'  => 'select',
		        'options'=> array(
			        '0' => esc_html__('Not override', 'schon'),
			        '1' => esc_html__('Enable overlapping', 'schon'),
			        '2' => esc_html__('Disable overlapping', 'schon'),
		        )
	        )

        )
    );

    $meta_boxes[] = array(
        'id'       => 'titlebar-options',
        'title'    => 'Title bar Options',
        'pages'    => get_post_types(),
        'context'  => 'advanced',
        'priority' => 'high',
        'fields' => array(
	        array(
		        'name'  => esc_html__('Hide Page Title Bar','schon'),
		        'id'    => $prefix . 'hide-page-title-bar',
		        'type'  => 'checkbox',
		        'options' => array(
			        "1" => "hide"
		        )
	        ),

	        array(
		        'name'  => esc_html__('Page Subtitle','schon'),
		        'desc'  => esc_html__('Showed in title bar','schon'),
		        'id'    => $prefix . 'page-subtitle',
		        'type'  => 'text',
	        ),

	        array(
		        'name'  => esc_html__('Titlebar background image', 'schon'),
		        'id'    => $prefix . 'titlebar-background-image',
		        'desc'  => esc_html__('Set an image background for the title bar.', 'schon'),
		        'type'  => 'file',
	        ),

	        array(
		        'name'  => esc_html__('Override padding top','schon'),
		        'desc'  => esc_html__('The padding above the content of title bar (If empty it use the default padding).','schon'),
		        'id'    => $prefix . 'titlebar-padding-top',
		        'type'  => 'text',
	        ),

	        array(
		        'name'  => esc_html__('Override padding bottom','schon'),
		        'desc'  => esc_html__('The padding below the content of title bar (If empty it use the default padding).','schon'),
		        'id'    => $prefix . 'titlebar-padding-bottom',
		        'type'  => 'text',
	        ),
        )
    );


	/*$meta_boxes[] = array(
		'title'      => 'Title bar fields',
		'taxonomies' => array('category','post_tag','product_cat'), // List of taxonomies. Array or string
		'fields' => array(
			array(
				'name' => __( 'Color', 'schon' ),
				'id'   => $prefix.'background_color',
				'type' => 'color',
			),
			array(
				'name' => __( 'Background image', 'schon' ),
				'id'   => $prefix.'titlebar_background_image',
				'type' => 'image_advanced',
			)
		)
	);*/

    return $meta_boxes;
}