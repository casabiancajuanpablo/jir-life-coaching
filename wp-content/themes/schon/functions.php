<?php
/**
 * schon functions and definitions
 *
 * @package schon
 */

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function schon_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'schon_content_width', 640 );
}
add_action( 'after_setup_theme', 'schon_content_width', 0 );

/*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on startertheme, use a find and replace
 * to change 'schon' to the name of your theme in all the template files
 */
load_theme_textdomain('schon', get_template_directory() . '/languages');

/**
 * Load admin option theme
 */
/*if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/admin/core/ReduxCore/framework.php' ) ) {
	require_once( get_template_directory() . '/admin/core/ReduxCore/framework.php' );
}*/
if ( !isset( $redux_demo ) && file_exists( get_template_directory() . '/admin/options/schon-config.php' ) ) {
	require_once( get_template_directory() . '/admin/options/schon-config.php' );
}

if(class_exists('Redux'))
	Redux::init( 'schon_options' );

/**
 * Include the hooks functions.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Include the helpers functions.
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Visual composer mapping of shortcodes
 */
require get_template_directory() . '/inc/vc_mappings.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';



/**
 * Custom Metaboxes.
 */
require get_template_directory() . '/inc/meta-box/meta-box.php';
require get_template_directory() . '/inc/metaboxes.php';








/**
 * Include the TGM_Plugin_Activation class.
 */
require_once( get_template_directory() . '/inc/class-tgm-plugin-activation.php' );

require_once( get_template_directory() . '/inc/tgm-plugin-configuration.php' );



