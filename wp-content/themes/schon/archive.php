<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package schon
 */

get_header(); ?>

	<div class="container">
		<div class="row">

			<div id="primary" class="content-area <?php echo schon_get_primary_class('sidebar-blog') ?>">
				
				<?php $main_classes = apply_filters('schon_main_classes', ''); ?>
				<main id="main" class="site-main <?php echo esc_attr($main_classes) ?>" role="main">

					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php 
								/* Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() ); 
							?>

						<?php endwhile; // end of the loop. ?>

						<?php the_posts_navigation(); ?>

					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->

			<?php schon_show_sidebar('sidebar-blog'); ?>

		</div><!-- .row -->
	</div><!-- .container -->
<?php get_footer(); ?>